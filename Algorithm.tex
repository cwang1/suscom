In this section, we outline the design space for our energy management policies. We first propose policies that focus on rigid parallel applications, which only rely on active power capping. This is because rigid applications cannot adjust the number of nodes being used while the application is running. While active power capping affects performance, it is transparent to the application, which enables rigid applications to make use of it. 
%Since inactive power capping deactivates nodes, it has the effect of periodically reducing the number of nodes an application is using; rigid applications cannot handle such reductions, as they will be perceived as failures. 
We then propose policies that leverage inactive power capping for elastic parallel applications. In addition to actively capping server power consumption by limiting the maximum CPU capacity, these policies stretch and contract an elastic application while it is running by activating and deactivating nodes to maintain a platform-wide power cap.  

%Note that to enforce a power cap, we must immediately force elastic applications to expand or contract based on changes in power. This differs from the typical scenario where elastic applications choose when to expand or contract based on their own utilization or the availability of resource, and have an unlimited amount of time to complete the expansion or contraction, e.g., by turning on or shutting down nodes.

\subsection{Rigid Parallel Applications}
\label{subsec:rigid-policy}

Many parallel applications are rigid, including most MPI applications. While MPI 2.0~\cite{mpi2} defines a dynamic process management scheme for elasticity, it is not commonly used by legacy MPI applications. Assuming a parallel application distributes the same amount of work to each node during each phase, and assuming each node is equivalent, the optimal policy to minimize running time is to equally distribute power among the nodes, such that each of the $N$ nodes' active power cap is set to $P_{cap} = P_{available}(t)/N$ at each time $t$, where $P_{available}$ represents the cluster-level available power for time $t$. We call this a \emph{balanced} energy management policy. Since the overall completion time of each phase depends on all nodes in each phase completing, equally dividing the power among nodes, akin to load balancing, is the optimal policy. If any node receives less than this allocation, it would serve as the bottleneck for completing each phase. The pseudo-code for balanced policy is shown in Algorithm~\ref{alg:balanced}.

\begin{algorithm}
\caption{Balanced policy}
\label{alg:balanced}
\begin{algorithmic}
\IF {$P_{available} / N > P_{idle}$}
        \STATE $P_{cap}= P_{available}/N$
\ELSE
        \STATE $N_{active} = \lceil P_{available}/P_{idle} \rceil$
        \STATE $P_{cap} = P_{available}/N_{active}$
\ENDIF
\end{algorithmic}
\end{algorithm}

%Here $P_{idle}$ denotes the power consumption of an idle server. 
Unfortunately, in practice, nodes do not always complete work at the same pace, because the jobs are not always equally distributed to worker nodes. For example, with a P-BFS, each node receives some partition of the graph, which it traverses.  Since partitioning the work \emph{a priori} into exactly equal partitions is not possible, node utilizations may fluctuate within a phase of execution based on the characteristics of their input data. 
%For instance, one node may receive a dense subset of the graph, while another may receive a sparse subset.  
Other differences between nodes may also contribute to differences in per-node utilization, such as differences in CPU capacity, e.g., 1.5Ghz versus 2.0Ghz, or in the set of background systems-level processes executing at any time. \CWRED{To illustrate, Figure~\ref{fig:fluctuations} shows the variance in power usage (due to differing node utilizations caused by task scheduling barriers) for two equivalent nodes, during the same execution of a large P-BFS using the reference MPI implementation of the Graph500 benchmark. This experiment is conducted without any power capping. The node power consumption is measured by an Intelligent Platform Management Interface (IPMI) card, which is an autonomous computer subsystem that is separated from the compute node's operating system.} The figure demonstrates that node power usage, as well as node utilization, varies significantly among the nodes during execution, and does not proceed in lock-step.  

\begin{figure}[!t]
\centering
\includegraphics[width=0.5\linewidth, keepaspectratio=true]{figures/power-variance.pdf}
\vspace{-0.2in}
\caption{Power fluctuations (every 10 seconds) for two worker nodes executing the reference MPI implementation of the Graph500 benchmark.}
%\vspace{-0.2in}
\label{fig:fluctuations}
\end{figure}

As a result of this variance, we can improve upon the pure balanced policy above by employing a \emph{dynamic} policy that continuously reallocates power within each phase based on node utilization.  In contrast, the ideal balanced policy above is a \emph{static} policy, since it adjusts node power caps based only on the available power and not real-time (measured) node power usage. Our dynamic policy begins each interval by setting the power cap such that it equally divides power among nodes initially, but then actively measures node utilization and power at short intervals, e.g., every second, and then adjusts active power caps based on the measured node utilization levels. 
\CWRED{Typically, active power capping has fast response time (about 20 ms, depending on system configurations~\cite{powernap}). In this article, we choose a reallocation interval duration of 1 second, because it takes 0.67 seconds on avereage (according to 100 measurements) for the power management system to read the energy usage data and distribute power allocation data to all the compute nodes.} 
%A finer granularity can further reduce energy consumption, however, the system can be less stable if the measurement data do not arrive on time. 
In particular, if any node has less than K\% CPU utilization, the policy reduces its active power cap such that it is above K\% utilization, while progressively increasing the active power cap of nodes operating above K\% utilization until they are below K\% utilization.  We set $K$ near, but less than, $100$\% to account for our relatively coarse per-second measurement intervals; our prototype sets $K=95$\%. We evaluate the impact of K in Section~\ref{sec:elastic_apps}. While our dynamic policy is aware of how CPU utilization changes with different active power caps based on profiling information known \emph{a priori}, at high utilizations, it does not know how much more power a node near 100\% utilization can effectively use.  Thus, the policy progressively increases a node's power cap every few seconds by a configurable threshold, e.g., $10$ watts (W).  In addition, the policy equally distributes the power taken from nodes (and available for reallocation) among all nodes that are operating above 95\% utilization. In essence, \emph{our dynamic variant of the balanced policy reduces wasted power by continuously (every second) taking power away from nodes that are not using it, and reallocating it to nodes that are using it.}  In our evaluation, we show that dynamically reallocating power significantly improves performance over the static policies. 

\CWRED{While the balanced policy is designed to work with homogeneous clusters, it can also be adapted to heterogeneous clusters by normalizing the idle power on each nodes, and therefore allocate power to the nodes in proportion to their idle power.}

\subsection{Elastic Parallel Applications and Overhead Power}

As discussed above, elastic parallel applications can use inactive power capping, in addition to active power capping, to match available power.  
%For example, if the available power is 50\% of a platform's maximum power, then an elastic application may meet the power cap by deactivating 50\% of the nodes, by setting the active power cap of each node to $P_{cap} = P_{max}/2$, or by some combination of the two techniques. 
Of course, inactive power capping incurs a transition overhead---order of seconds-to-minutes depending on the platform---relative to active power capping, which is effectively instantaneous.  Since the transition time represents wasted energy, which does no useful work, any use of inactive power capping must provide a benefit that exceeds the cost of the overhead. As SSD becomes more common on servers, the transition time is expected to be significantly reduced.

A straightforward policy that uses inactive power capping is to greedily allocate available power at the beginning of each interval such that the number of active nodes $N_{active} = \lceil P_{available}/P_{max} \rceil$, where each node's active power cap is set to $P_{max}$.   This policy is shown in Algorithm~\ref{alg:greedy}.  Such a policy could also be used in conjunction with the dynamic policy above to reallocate power within each interval by adjusting each node's active power cap.  
%Relative to active power capping, deactivating nodes with inactive power capping is beneficial in reducing the fraction of the available power that contributes to an active node's overhead.  
%For example, since each node's idle power is roughly 50\% peak power, simply activating a node without doing any useful work consumes a significant amount of power.  
%This idle power consumption effectively represents wasted energy that is due to powering non-energy-proportional components, such as the motherboard, disk, NIC, memory, etc.

\begin{algorithm}
\caption{Greedy policy}
\label{alg:greedy}
\begin{algorithmic}
\STATE $N_{active} = \lceil P_{available}/P_{max} \rceil$
\FOR{$n \gets 1, (N_{active} - 1)$}
	\STATE $P_{cap}[n] = P_{max}$
\ENDFOR
\STATE $P_{cap}[N_{active}] = P_{available} - P_{max}\times (N_{active} - 1)$
\end{algorithmic}
\end{algorithm}

Here, we identify the idle power consumption as \emph{overhead power} for each active node, and the power usage above idle as the node's \emph{effective power}, since only the effective power contributes to actual program execution. While the dynamic balanced policy works well if nodes cannot be deactivated, it incurs high overhead power across all nodes, since all nodes must remain active regardless of the available power. As an example, Figure~\ref{fig:power-capping-comparison} illustrates three energy management polices.  From the figure, consider a cluster with $4$ nodes, each with an idle power of $80$W and a maximum power of $150$W.  If the total available power is $400$W, the available power is $100$W for each node, therefore, the total effective power for cluster is $80$W, i.e., only 20\% of the available power above idle is being used effectively to perform computational work. By contrast, a \emph{greedy} policy would only activate three nodes, which increases the fraction of effective power to $160$W, i.e., 40\%, and devotes more power to doing useful work. In addition, we also define a dummy policy, called \emph{greedily balanced}, by combining the balanced and greedy approach, such that three nodes are activated and the available power is equally distributed across the three active nodes. We compare the performance of these policies with a simple experiment on a four-node cluster with Dell R720 servers, running Graph500 P-BFS application. We compare the application runtime in Figure~\ref{fig:algo-comparison}. As expected, the greedy policy, which uses inactive power capping, is more energy-efficient compared to the balanced policy, and results in better performance and lower runtime. The greedily balanced policy further reduces runtime because it combines the advantage of both balanced and greedy policies by reducing the overhead power and balance power allocation to the worker nodes.
%
%\begin{figure}[!t]
%\centering
%\includegraphics[width=0.6\linewidth, keepaspectratio=true]{figures/power-capping-comparison.pdf}
%\vspace{-0.85cm}
%\caption{Overhead power analysis with different management policies.}
%\vspace{-0.45cm}
%\label{fig:power-capping-comparison}
%\end{figure}
%
%\begin{figure}[!t]
%\centering
%\includegraphics[width=0.6\linewidth, keepaspectratio=true]{figures/algo-comparison.pdf}
%\vspace{-0.45cm}
%\caption{Graph500 runtime analysis with different management policies.}
%\vspace{-0.45cm}
%\label{fig:algo-comparison}
%\end{figure}


\begin{figure}[!t]
\centering
\subfigure[]{
\vspace{-0.4cm}
\includegraphics[width=0.56\linewidth, keepaspectratio=true]{figures/power-capping-comparison.pdf}
\label{fig:power-capping-comparison}
 }
 \subfigure[]{
\includegraphics[width=0.38\linewidth, keepaspectratio=true]{figures/algo-comparison.pdf}
\label{fig:algo-comparison}
 }
  \centering
 \vspace{-0.2cm}
\caption{Power management policies (\ref{fig:power-capping-comparison}) and runtime analysis (\ref{fig:algo-comparison}) for reference Graph500 application.}
\vspace{-0.2cm}
\label{fig:policy-comparison}
\end{figure}

Based on the above policies, in order to further reduce the overhead power, we define an \emph{agile} policy shown in Algorithm~\ref{alg:agile} that determines the optimal number of active nodes at the beginning of each interval.  To do this, we compute the overhead power associated with each possible set of active nodes, from $1$ to $\lceil P_{available}/P_{idle} \rceil$, based on the available power. Here, we assume that each node operates at near 100\% utilization; in this case, the policy activates the number of nodes that minimize overhead power.  Once these nodes are active, the agile policy equally distributes the available power among the active nodes at the start of the interval; the policy may then employ the dynamic balanced policy, described in~\ref{subsec:rigid-policy}, among the active set of nodes within each interval. 

\CWRED{Both greedy and agile policies can be naturally applied to heterogeneous clusters. In such case, the greedy policy concentrates the available power to a set of nodes that contains the largest $P_{max}/P_{idle}$ value. While for agile policy, the procedure remains unchanged, by iterating the overhead power calculations, given a pre-measured, normalized set of overhead power values. }

%Of course, the length of each interval, the time to transition to and from the inactive state, and the frequency of these transitions dictates the overall performance of the policy.  We further discuss the these parameters in Section~\ref{sec:evaluation}. 

\begin{algorithm}
\caption{Agile policy}
\label{alg:agile}
\begin{algorithmic}[1]

\STATE $N_{active} = \lceil P_{available}/P_{max} \rceil$
\STATE $P_{minOverhead}= \frac{\sum^{N_{active}}_1 {P_{idle}}}{\sum^{N_{active}}_1{P_{max}}}$
\FOR{$n \gets 1,  \lceil P_{available}/P_{idle} \rceil$}
	\STATE $P_{overhead} = \frac{\sum^n_1 {P_{idle}}}{\sum^n_1{P_{max}}}$
	\IF {$P_{overhead} < P_{minOverhead}$}
		\STATE $P_{minOverhead} = P_{overhead}$
		\STATE $N_{active} = n$
	\ENDIF
\ENDFOR
\STATE $P_{cap} = P_{available}/N_{active}$
\end{algorithmic}
\end{algorithm}

\subsection{\CWRED{Energy Management Policy Analysis}}
\CWRED{In this section, we provide an analysis on the example presented in Figure~\ref{fig:policy-comparison}. We take a homogeneous MPI cluster as an example. Given the total workload $W$, the compute task execution speed is $s$, with job communication time $T_{comm}$, and task execution time $T_{exe}$. 
We set the total number of nodes contained in cluster as $N$. At any given time, the number of active node is $n$, with CPU frequency on node $x$ as $f_x$. By estimation, the CPU processing speed is proportional to the CPU frequency, i.e., $s\propto f$, which is proportional to the effective power. Therefore, the effective CPU speed can be represented by:}
\begin{equation}
s_{CPU}=s_{max}\cdot \frac{P_{curr}-P_{idle}}{P_{max}-P_{idle}}
\end{equation}

\CWRED{The total task execution time is represented by the following equation.}
\begin{equation}
T_{total} = \sum_{phase=1}^{M}(T_{exe} + T_{comm})
\end{equation}
\CWRED{Typically, for each phase, the MPI program waits until all the worker nodes finish their portion of job execution before continuing to the next phase. Therefore, an unbalanced MPI cluster significantly impacts application runtime due to the blocking behavior of the slowest worker node.}

\CWRED{Assume the overall renewable power signal is $P_{available}(t)$, at any time, the instant overall power is $P_{total}$. In this case, for balanced policy, each node gets power $P_x=P_{total}/N$. For greedy policy, for the first $n-1$ nodes, each one gets $P_{max}$, the last one gets $P_x=P_{total}-P_{max}\cdot (n-1)$, which typically becomes the slowest worker node. For agile, all the nodes get power $P_x=P_{total}/n$. Here agile policy balances the power consumption, while leaving no power-hungry nodes. Therefore, it achieves lowest application runtime comparing to other policies.}

\begin{comment}
\noindent{\bf Busy-waiting.} Busy-waiting happens when a process continually tests a variable or process, looking for confirmation such that the current process can continue. Busy-waiting is usually caused by barriers in MPI applications and it increases the power utilization since the waiting process also increases CPU utilization. Fortunately, busy-waiting is not common in MPI applications, depending on the implementation, since there is usually a timer between two message exchanges, and multiple messages can be combined into single message if they are sent within a short time. In addition, the busy-waiting nodes usually continue to use I/O, but they are less CPU intensive, since waiting does not check if resources become available for every CPU clock cycle. Furthermore, busy-waiting is application-specific, our policies look into the overall power and CPU usage. An improved parallel application with less busy-waiting can leave more room for the optimization from our policies.
\end{comment}

\noindent {\bf Summary.}  Table~\ref{tab:summary} summarizes the design space for our energy-agile policies. The basic idea for our power policies is to determine the optimal energy reallocation based on an application's characteristics, e.g., rigid vs. elastic, and the available power capping mechanisms. The balanced policy only utilizes active power capping by limiting maximum CPU capacity and therefore is suitable for rigid applications.  With elastic applications, greedy and agile policies can make use of both active and inactive power capping by determining the optimal subset of nodes that minimizes overhead power. For all three energy management policies, we have two variations: the static variation, i.e., keep power allocation unchanged once an application starts, and the dynamic variation, i.e., keep tracking CPU utilization and reallocate power cap.

\begin{table}[!t]
%\newcommand{\tabincell}[2]{\begin{tabular}{@{}#1@{}}#2\end{tabular}}
\centering
\resizebox{4in}{!}{
\begin{tabular}{| c | c | c | c |} 
\hline
Metric & Balanced & Greedy & Agile\\
\hline
\hline
Power Capping & Active & Active \& Inactive & Active \& Inactive\\ \hline
Best Application & Rigid & Elastic & Elastic\\ \hline
Cluster Type & Homogeneous & Heteo- \& Homogeneous & Heteo- \& Homogeneous\\ \hline
\end{tabular}
}
\caption{\CWRED{Summary of design space for energy-agile policies.}}
\vspace{-0.4cm}
\label{tab:summary}
\end{table}



%% Elastic application may have limits on its elasticity.  Will need to reallocate work and/or transfer data.  Must include a centralized repository of data. 






















%To do this, we simply compute based on the available power, the power overhead associated with each possible set of active nodes (from $1$ to $N$), assuming each node operates near 100\% utilization, and activates the number of nodes that result in the minimum overhead power.  


%If the time to transition is 
% \MZ{Interactive power capping could become more interesting in the future when the dynamic power range between active and suspend mode becomes larger by a significantly reduced power consumption in suspend mode. Introduce static and dynamic strategies.}

%Before introducing the algorithms, we define the term of ``Wasted Power". In this paper, we refer the wasted power as the server idle power, and/or consuming higher amount of power than its needed peak power consumption. For the servers we mentioned above, if current power reading is 100 W, only 20 W is counted towards the effective power usage, i.e., power consumed towards calculation, thus 80 W is wasted. Also the maximum effective power is 150 W, if we give power budget beyond this max power, the excessive power is wasted. For example, if the server is currently consuming 240 W power, then is wasting 96 W power.

%On the other hand, typical cluster applications, like Graph 500, are required to exchange information with other nodes periodically, thus the slowest node decides the overall runtime of the application.

%Therefore, here we would like to minimize the wasted energy while maintaining a balanced power distribution which is as close to the peak power as possible. 

%\section{The Minimum Wasted Power Algorithm} %system?
%\label{sec:algorithm}

\begin{comment}


ElasticCloud is a management system that could dynamically adapt to the available power, and find the best suitable strategies to allocate power into servers. This adaptation process is especially useful when the data center is powered by intermittent power sources, e.g., renewable energy.

In this section, we start from a few simple algorithms, then we show how ElasticCloud can take the advantages of these algorithms and thus find the best possible solution in a relatively short time.

\subsection{Greedy Power Allocation}
Given a power budget that is less than the peak data center power demand, one approach is to power as many servers running at full capacity as possible, then allocate the residual into the rest of servers. Through the greedy algorithm, the limited number of servers can get optimal power for their operation, this can be considered as an extreme case of focusing power into small area

\subsection{Balanced Power Allocation}
On the other hand, the balanced algorithm distributes power into all the cluster nodes. This provides each of the nodes equally limited available resources, which is usually much less than the peak power.

\subsection{The Overall Power capping Algorithm}
\label{sec:optimizer}
Algorithm~\ref{alg:optimizer} shows the procedure of our power optimization algorithm. Here we consider both the greedy and balanced approaches for our power optimization problem. We assume there are in total N nodes in cluster. At job submission time, we start from allocating power to 1 node, then gradually increase the number of nodes that are powered on, and equally distribute power into these nodes, until we find the best number of nodes that can be left on. 

This algorithm linearly scan the power performance from 1 node till the number of nodes when each node can only get idle power. The worst case is to scan all the nodes in the cluster, thus it achieves runtime of $O(n)$.

\begin{algorithm}[h]
\caption{Power Optimizer Algorithm}
\label{alg:optimizer}
\begin{algorithmic}[1]
\REQUIRE ~~\\ %Input
Total available power $P_{total}$;\\
Idle power IDLE;\\
Peak power MAX;\\
Total number of nodes N;\\
\ENSURE ~~\\ %Output
Overall power cap cluster wide $overallCap$;\\
Number of nodes that can be turned on $n$;\\

\STATE $minWaste=IDLE\times N$
\STATE $n=0$
\FOR{each $i$ in $[1,N]$}

	\IF{$\frac{A}{i} <IDLE$}
		\STATE continue;\
			
	\ELSIF{$\frac{A}{i}>MAX$}
		\STATE $waste=\frac{A}{i}-MAX$;
	\ELSE
		\STATE $waste=IDLE$;
	\ENDIF
	\IF{$waste<minWaste$}
		\STATE $minWaste=waste$;
		\STATE $n=i$;
	\ENDIF
	
	\STATE  $overallCap=\frac{P_{total}}{n}$
\ENDFOR
\STATE return $n$, $overallCap$
\end{algorithmic}
\end{algorithm}

%\subsubsection{The Power optimization Algorithm with Constant Runtime}

\subsection{The Dynamic Energy Allocation Policy} 
The algorithm in Section~\ref{sec:optimizer} aims to find a rough power cap that is similar for all the nodes in cluster. However, in reality, not every node has exactly the same CPU, networking and power usage. Thus we present the feedback power control algorithm in this section. 

To find if any of the nodes in cluster has a higher power cap than it should have, we scan the power consumption of each node, then compare the actual power consumption to the overall power cap: if the nodes are consuming less power than the power cap value, then we set the power cap to the actual power consumption value; if the nodes consume the power right at the power cap value, we save the state and use the power micro adjustment method in Section~\ref{sec:microAdjustment} to find the best power cap for these nodes. 

On the other hand, to ensure the best possible performance, our goal is to find the balanced point that all nodes are able to coordinate at balanced power state while the cluster is able to run on its best overall performance. Following the feedback power allocation, we are able to get a list of servers that are operating at a near-threshold point. For these nodes, we slightly increase the power cap, then keep tracking of their energy consumption to see if it actually increases with the elevated power cap. If the performance does not increase, we then go back to the feedback control state; if the power consumption follows up, we then continue slightly increasing power cap util we find a fitted power cap.

Figure~\ref{fig:graph500-capping} shows an illustration of our power capping policy at granularity of 10 seconds. We run the Graph 500 benchmark on our 6-node cluster, and record both the power cap and actual consumed power on one node. As can be seen, the actual power consumption of server varies quickly when running data intensive application, from 100 W to up to 240, our capping policy is able to closely follow the power utilization. At some points, the ElasticCloud tries to increase the power cap, however, the actual power consumption goes down, thus the algorithm is able to quickly adjust the power cap to reduce the wasted energy.

\begin{figure}[!htb]
\centering
\includegraphics[width=1\linewidth, keepaspectratio=true]{figures/graph500-capping.pdf}
\caption{The capping control machanism}
\vspace{-.2in}
\label{fig:graph500-capping}
\end{figure}


\begin{algorithm}[h]
\caption{Feedback Power Control Algorithm}
\label{alg:feedback}
\begin{algorithmic}[1]
\REQUIRE ~~\\ %Input
Overall power cap $overallCap$;\\
Actual power cap on each node n $actualPower_n$;\\
\ENSURE ~~\\ %Output
Power cap for each node $$;\\

\FOR{each $n$ in $[1,N]$}

	\IF{$actualPower_n<overallCap$}
		\STATE continue;\
			
	\ELSIF{$\frac{A}{i}>MAX$}
		\STATE $waste=\frac{A}{i}-MAX$;
	\ELSE
		\STATE $wast=IDLE$;
	\ENDIF
	\IF{$waste<minWaste$}
		\STATE $minWaste=waste$;
		\STATE $n=i$;
	\ENDIF
\ENDFOR
\end{algorithmic}
\end{algorithm}


%\subsection{Energy Micro Adjustment}
%\label{sec:microAdjustment}
%The energy allocation with feedback loop is able to find the wasted energy space when nodes are operating on power that is lower than the power cap. 
\subsection{The Power Variation Adaptation}
Our power management scenario is built with the goal of dynamically controlling power state so that the cluster can be adaptive to the power supply that is {\it i)} less than able to power the whole cluster at its full capacity and/or {\it ii)} variable along with time. This is especially important when the cluster is powered by renewable energy resources which are not stable according to the natural environment.

Our system is able to quickly response to the power resource changing. However, seeking for a good power cap switching interval is important. A short power switching interval usually means higher adaptivity and more power saving; whereas a longer power switching interval can provide lower overhead of switching servers into and out of power saving state. To better understanding this problem and provide a reasonable granularity, we provide a discussion based on experimental results in the evaluation section.

\subsection{Failure Handling}

\subsection{Power Capping Accuracy}
The key factor for the capped power allocation strategy is the accuracy of power capping. After the adjustment of the power optimizer, the power control united must be able to accurately cap the power utilization in order to ensure the overall performance. Therefore, we perform a series of measurements the performance of power capping mechanism.

Starting from 0 W, we gradually increase the power cap until it reaches a stable state. The results is shown in Figure~\ref{capping-accuracy}. 

\begin{figure}[!htb]
\centering
\includegraphics[width=1\linewidth, keepaspectratio=true]{figures/capping-accuracy.pdf}
\caption{Power capping accuracy}
\vspace{-.2in}
\label{fig:capping-accuracy}
\end{figure}


\subsection{System Architecture}
The ElasticCloud is a system for dynamically controlling power capping strategies in order to adapt to the variations of energy resources of data centers. ElasticCloud contains four modules: the power optimizer, power monitor, power control, and power distribution unit (PDU). The power monitoring module reads power meter from the PDU, then periodically report to the optimizer. The optimizer is responsible to find the best power allocation strategy, considering the system available power, current power capping and real power consumption. The optimizer sends calculation results to the power control module, who will finally put parameters into real new power cap to the data center nodes.
 
\begin{figure}[!htb]
\centering
\includegraphics[width=1\linewidth, keepaspectratio=true]{figures/SystemDiagram.pdf}
\caption{The system architecture}
\vspace{-.2in}
\label{fig:systemArchitecture}
\end{figure}

\end{comment}






\begin{comment}

The main goal of this paper is to propose and evaluate a set of algorithms to improve the power utilization efficiency in data centers, when limited amount of power budget is assigned to the cluster. In this section, we discuss the background information, the definition of the problem we are trying to solve, and then we present an interesting motivating example to show the necessity and effectiveness of power allocation strategies.

\subsection{Server Power Capping}
As a concept brought out in 2009, server power capping has been a commonly discussed technique in today's data center technologies. Power capping allocates fixed amount of power budget, allowing the system power consumption to stay below the defined power capping value. The power capping It can be broadly used to reduce the data center over-provisioning problem by setting a power limit below the peak power.

The traditional way of dealing with the insufficient available power situation is to turn off nodes or put servers to sleep mode, i.e., inactive power capping. However, with the help of active power capping, we are given higher flexibility control the actual power consumption of each server. As a trade-off, when power caps applies, the computational capacity of server reduces. Thus finding the best suitable power caps across the data center can be essential to optimize the overall performance.

\subsection{Demand Response Problem}
Another key factor for renewable energy powered data centers is demand response. Here in green data centers, one of the most concerned problem is how to make use of the available power budget to maintain the best possible data center performance.  More formally, for each period $t$, the sum of generated power $P_t$ and stored battery power in the previous period $B_{t-\tau}$ must be more than the actual power consumption in period $t$, $C_{t}$, i.e., $P_t+B_{t-\tau}-C_t\geq 0$. The generated power is a given signal depending on the power source, and varies with time. We aim to decide how to control the actual power usage over certain time interval based on how much energy is available. At each $t-\tau$ time interval, a power budget is given, we need to meet over the next time interval, $\tau$. The size of $\tau$ is based on the available energy storage, so that we can store the remaining energy from the previous time interval and then use the energy in the next interval. For smooth energy planning, we need an energy buffer that stores the energy from the previous time interval, and at the beginning of the next time interval we use it, so that we know how much energy we have.

\subsection{Parallel Computing and MPI}
\CW{we need to mention graph500 because it'll be used for the example}
For parallel applications, one of the most common used library is the Message Passing Interface (MPI). MPI is a standard for implementing distributed-memory parallel applications. The common architecture of MPI contains a single master node and a group of worker nodes. At the beginning of the application, the master node divide task into pieces, each of the worker nodes gets a portion of the overall computing problem. The worker nodes periodically synchronize with master node, and get new portion of the task. Usually users are not allowed to change the number of worker nodes after the program starts. The MPI-2.0 defined dynamic process management scheme, i.e., elasticity, however, currently the non-elastic MPI is still commonly used for most of the existing parallel applications.

Our system can be generally used for most parallel applications. To evaluate our system performance, we hire the commonly used Graph 500 benchmark~\cite{graph500}. Graph 500 benchmark was announced in 2010, and is designed to rate the high performance computers (HPC) when running large scale data intensive applications. In complement of other float point oriented benchmarks, such as High Performance Linpack, Graph 500 runs distributed breadth-first search in large graphs. It focuses more on the data communication throughout the system. In our system, we consider the cluster as a whole, thus Graph 500 fits our goal better. We use the Graph 500 implementation based on MPI. 

To provide a baseline for our comparison of power allocation strategies, we show the graph 500 runtime of different graph scales in Figure~\ref{fig:graph500-comparison}. This experiment runs on a 6 node mini cluster, and all the nodes runs under full power. The scale of Graph 500 is in terms of the logarithm base two of the number of vertices, e.g., the graph scale of 20 contains $2^{20}$ vertices.

\begin{figure}[!htb]
\centering
\includegraphics[width=1\linewidth, keepaspectratio=true]{figures/graph500-comparison.pdf}
\caption{The runtime of graph 500 when operating at full power}
\vspace{-.2in}
\label{fig:graph500-comparison}
\end{figure}

\subsection{Power Monitoring and Allocation Strategies}
Our main interest is to propose an efficient power allocation algorithm for renewable energy situation, where available energy changes over time, and are not always fully available to power the entire data center. From the service providers' point of view, a natural objective of a power allocation algorithm is to provide as much computational resources as possible while keep adaptive to the variation of available power resources. In this section, we start with a simple example on a setup with 4 servers, we aim to show how varying power allocation strategies can impact the actual performance of the cluster. As a starting point, we introduce two intuitive algorithms: \emph{greedy} and \emph{balanced}, then we introduce how we can achieve better power utilization by improving based on these two algorithms.
%Minimize the waste such that all of the nodes have the same power budget which is close to the peak power.

\subsubsection{``Wasted Power"}
Before introducing the algorithms, we define the term of ``Wasted Power". In this paper, we refer the wasted power as the server idle power, and/or consuming higher amount of power than its needed peak power consumption. For the servers we mentioned above, if current power reading is 100 W, only 20 W is counted towards the effective power usage, i.e., power consumed towards calculation, thus 80 W is wasted. Also the maximum effective power is 150 W, if we give power budget beyond this max power, the excessive power is wasted. For example, if the server is currently consuming 240 W power, then is wasting 96 W power.

On the other hand, typical cluster applications, like Graph 500, are required to exchange information with other nodes periodically, thus the slowest node decides the overall runtime of the application.

Therefore, here we would like to minimize the wasted energy while maintaining a balanced power distribution which is as close to the peak power as possible. 

\subsubsection{Motivating Example}
By using the native Dell Active Power Control (DAPC) application, which provides OS-independent power management, we are able to measure the fine-tuned power consumption of the server, and also control the available power cap for each server. The active power capping is achieved by assigning the servers synthetic CPU workload to limit the available CPU capacity.

By measuring the Graph 500 performance, we show how the active power capping can impact the application performance. For simplicity, we start with an example with 4 servers. Based on the measurements of Graph 500, the nominal peak power consumption without power capping is 150 W. We assume that the total available power is 400 W. 

We start from a simple example to illustrate our fundamental method. Here we use 3 power capping strategies, which are summarized in Figure~\ref{fig:powerCapping}. The Power monitoring and allocation strategies are discussed in detail in Section~\ref{sec:algorithm}.
\begin{itemize}
\item The first strategy gives the maximum number of servers the maximum possible power, i.e., allocate 150 W to first two servers, then allocate the rest of power to the third server (100 W). Put the rest of the servers to sleep. 
\item The second strategy allocate power equally to all the servers. In this case, none of the servers can get enough power to support maximum CPU speed.
\item For the third strategy, we integrates the above two, first calculate how many servers can be supported by available power, then equally distribute the power to those servers.
\end{itemize}

\begin{figure}[!htb]
\centering
\includegraphics[width=1\linewidth, keepaspectratio=true]{figures/power-capping.pdf}
\caption{The power capping stretegies}
\vspace{-.2in}
\label{fig:powerCapping}
\end{figure}

The above three cases are tested under different power scenarios. We run Graph 500 for 10 iterations for each data point. The available power for each server is represented on x axis, the average runtime of 10 iterations are shown on y axis. As can be observed, the third strategy shows us significantly less runtime then the other two. This clearly states that with same amount of available power, using appropriate power allocation strategy can make significant difference.

This behavior can be predicted by our wasted power calculation. For the first strategy, the wasted power for first 3 nodes is 240 W; for the second strategy, the wasted power is 320 W. For the third strategy, the wasted power is also 240 W, however, the power is more evenly distributed across the nodes, which fits the nature of most cluster applications: the total performance is usually decided by the slowest node.

\begin{figure}[!htb]
\centering
\includegraphics[width=1\linewidth, keepaspectratio=true]{figures/algo-comparison.pdf}
\caption{The power capping stretegies}
\vspace{-.2in}
\label{fig:algo-comparison}
\end{figure}

\end{comment}

