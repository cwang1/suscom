
\subsection{System Architecture} 
We develop a prototype power manager to evaluate the presented policies.  The power manager, depicted in Figure~\ref{fig:systemArchitecture}, has three functions: monitoring power consumption and available power, executing energy management policies, and deploying these policies by setting power cap.  The prototype monitors nodes use an out-of-band server management card that supports IPMI protocol, which enables power monitoring at a 1Hz resolution at 1W granularity.  We cap active power in our prototype by controlling CPU power states, e.g., via Dynamic Voltage and Frequency scaling and changing C-states.  Since the prototype servers do not support inactive power capping via ACPI's Suspend-to-RAM (S3) state, we emulate S3 in our experiments by disconnecting nodes from the network.


%OLD text: We implement a prototype energy-agile cluster using 65 servers on CloudLab, a testbed infrastructure for research on the future of cloud computing. Each server is with Eight 64-bit cores and 64GB memory. Each server runs Linux kernel 3.13.0-24-generic x86\_64 with Graph500 and MPI installed. Because most of the data centers are not dedicated for single application, to create a realistic scenario, we installed two other MPI applications as interference applications: Weather Research and Forecasting (WRF) and Jacobi iteration. The WRF Model is a next-generation mesoscale numerical weather prediction system designed for both atmospheric research and operational forecasting needs, the Jacobi algorithm is a well known numerical method used to solve linear algebraic systems of n equations with n unknowns. These two interference applications run along with Graph500. We measure the performance of the energy-agile policies using standard Graph500 performance metrics: the runtime and graph scale. The servers we used include IPMI control interface to monitoring the power consumption at a resolution of 1W each second. Unfortunately, the servers do not support ACPI's Suspend-to-RAM (S3) state, which is generally not available in any server-class node. Thus, for actual inactive power capping, we use ACPI's Suspend-to-Disk (S4) state, which has a transition time of $\sim$90 seconds due to a series of required pre-boot tests.  As a result, we emulate the use of S3 in our experiments by disconnecting nodes from the network without deactivating them; this enables us to configure (and emulate) any inactive power state transition time.  

 \begin{figure}[!t]
\centering
\includegraphics[width=0.65\linewidth, keepaspectratio=true]{figures/SystemDiagram.pdf}
\vspace{-0.4cm}
\caption{Our prototype system architecture and power manager.}
\vspace{-0.4cm}
\label{fig:systemArchitecture}
\end{figure}

\subsection{Reference Applications}
We prototype our policies on a real cloud testbed with all dedicated servers, and evaluate the policies using three parallel, MPI based applications: Graph500, WRF and Jacobi, with single process on each node. \CW{We use Graph500 as the main benchmark in this article, since it is a widely-accepted platform benchmark for parallel platforms. Graph500 runs with a moderate input graph scale of 26, i.e., the input graph contains $2^{26}$ vertices.} The Weather Research and Forecasting (WRF) application~\cite{wrf} is a commonly used, mesoscale numerical weather prediction application. \CW{In our prototype, we make use of WRF with an input scale of 1km$\times$1km, and time step of 60 seconds.} Jacobi is a numerical method for solving linear algebraic systems of \emph{n} equations with \emph{n} unknowns.  \CW{We make use of an MPI based parallel Jacobi implementation with random input matrix size $n=10000$.} The WRF power consumption behavior is relatively stable; while the communication pattern of Jacobi is similar to Graph500 with synchronization barriers that make power consumption more variable. By default, the three MPI applications are rigid, i.e., we cannot adjust the number of worker nodes during its execution. However, we also experiment with an elastic variant of Graph500 by applying a method proposed by Raveendran et al.~\cite{6008941} to transform a rigid parallel task into an elastic one. \CW{In this case, the elastic MPI applications add a \emph{decision layer} to decide the amount of jobs to be performed for each iteration, and collect current result and automatically redistribute the tasks to the new number of nodes for the next iteration. }

\begin{figure*}[!t]
\centering
\subfigure[]{
\includegraphics[width=0.31\linewidth, keepaspectratio=true]{figures/solar-energy.pdf}
\label{fig:solar-energy}
 }
 \subfigure[]{
\includegraphics[width=0.31\linewidth, keepaspectratio=true]{figures/wind-energy.pdf}
\label{fig:wind-energy}
 }
 \subfigure[]{
\includegraphics[width=0.31\linewidth, keepaspectratio=true]{figures/price-energy.pdf}
\label{fig:energy-n-price}
 }
\centering
\vspace{-0.3cm}
\caption{The solar (\ref{fig:solar-energy}) and wind power (\ref{fig:wind-energy}) generated over a day, as well as a power signal based on using a fixed budget to purchase electricity at real-time prices in the five-minute spot market (\ref{fig:energy-n-price}).}
\label{fig:energy}
\vspace{-0.3cm}
\end{figure*}


\subsection{Input Power Signal} 
Our experiments utilize power signals from real solar and wind deployments, as well as signals based on real power prices from the wholesale electricity market. For each power signal, we select a representative day-long period with average power readings every minute.\footnote{For solar power, we selected an early Fall day, September 28, 2014 from 12am to 11:59pm. For wind power, we selected a typical spring day, April 26, 2014 from 12am to 11:59pm.} \CW{The power signal for the solar and wind traces are collected from the solar panel and wind turbine deployments located in western Massachusetts, United States, and are shown in Figure~\ref{fig:solar-energy} and Figure~\ref{fig:wind-energy}.} For the energy price trace, we use the five-minute spot price from the New England Independent System Operator (ISO) on October 1, 2014 from 12am to 11:59pm.  In this case, we assume our system has a fixed budget for purchasing energy, such that during a low price period it may purchase more power, and during a high price period it must purchase less.  The resulting power signal is shown in Figure~\ref{fig:energy-n-price}. Finally, to make a fair comparison among different power signals, we normalized all three traces such that they have the same average power. 


