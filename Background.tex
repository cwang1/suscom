In this section, we present our problem statement, as well as provide background information on power capping techniques and our general model for parallel tasks.

\subsection{Problem Statement}

Our work assumes a parallel application that is subject to dynamic power constraints, which may arise from either the use of renewable energy sources or participation in a utility demand response program. Our goal is to finish executing a parallel application as fast as possible given these dynamic constraints.  More formally, we aim to minimize a parallel task's running time given a power signal $P(t)$, which dictates an average power cap over each interval $(t-\tau,t]$.  Here, $\tau$ is the length of each interval, which we assume is dictated by the amount of  energy storage capacity available.  Since energy storage is expensive to install and maintain, smaller values of $\tau$ are better.   The application defines its power constraint for each interval $(t,t+\tau]$ based on the energy it stored during the previous interval $(t-\tau,t]$.  Thus, the power constraint at each interval is known at the beginning of the interval.  

\subsection{Power Capping Mechanisms}

Given an average power budget for each interval $\tau$, we must distribute the available power among $N$ nodes executing a parallel application.  We leverage existing node power capping mechanisms, which ensure a node's power usage does not exceed a set threshold, to enforce a given distribution of power.  Power capping may be either active or inactive.  \emph{Active power capping} uses Dynamic Voltage and Frequency Scaling (DVFS) or C-state throttling, which rapidly toggles processors between low-power idle C-states, to cap power without deactivating a node. The advantage of active power capping is that it keeps nodes active (albeit at a degraded performance level), is transparent to application software, is highly responsive (as power cap changes occur near instantly), and imposes a low overhead to transition power states.  However, the primary disadvantage of active power capping is that it typically only targets CPU power, which accounts for only a fraction of node power usage, and thus its dynamic power range is limited~\cite{fawn}.  
%In practice, active power capping is typically only able to lower a node's power usage to at most 50\% of its peak power.  

In contrast, \emph{inactive power capping} transitions nodes to an inactive state using either ACPI's Suspend-to-RAM (S3) or Suspend-to-Disk (S4) state.  While inactive power capping is able to reduce a node's power usage to near zero, it incurs a high temporal overhead to transition states, e.g., tens of seconds for S3 and minutes for S4 for servers with hard disks, and prevents any work allocated to an inactive node from making forward progress.  The temporal overhead also wastes energy, since the node consumes energy while transitioning, but performs no computation.  Importantly, inactive power capping is visible to applications, which must gracefully handle the dynamic loss and addition of nodes.  
%Due to its high overhead, inactive power capping is only used when capping the power of clusters, since it enables a wider dynamic power range than when only using active power capping~\cite{energy-prop-ensemble}. 
While inactive power capping is not as commonly used as active power capping, we show its potential to improve the system performance under variable power, especially with today's development of higher disk speeds and SSDs.

\subsection{Parallel Task Rebalancing Model}
Our energy management policies described in Section~\ref{sec:algorithm} determine how to distribute the available power for each interval $[t-\tau,t)$ given the two power capping techniques above: active power capping, which may instantaneously adjust power consumption between some $P_{max}$ and $P_{idle}$ while keeping a node active, and inactive power capping, which incurs some overhead $T$ on the order of seconds to reduce power consumption to zero, but completely deactivates the node. The goal of our policies is to minimize the running time of a parallel task.  
%Parallel tasks may exhibit a wide variety of communication patterns and inter-node dependencies during their execution.  
Below, we illustrate key elements of our energy management policies using a representative example---a parallel breadth first search (P-BFS) algorithm---which serves as a building block for designing policies for tasks with more complex communication patterns. 

P-BFS and other graph algorithms are frequent subproblems in a variety of data-intensive HPC analytics applications, which is the primary reason P-BFS was chosen as the foundation of the recently introduced Graph500 benchmark~\cite{graph500}. 
%In practice, P-BFSs are often massive in scope, searching graphs with tens of billions of edges and vertices on platforms with tens of thousands of cores~\cite{parallel-bfs}. 
Importantly, since the classic P-BFS~\cite{pbfs-original} implementation is ``level-synchronous,'' it requires all-to-all communication among nodes at each phase of graph searching to determine whether a vertex has already been visited, i.e., by transmitting all remote edges on each node to the node that owns them, or transmitting all remote edges to a master node.  Most non-embarrassingly parallel tasks will include similar types of barriers to synchronize their operation between phases.

While all-to-all synchronization barriers, which represent a dependency between each pair of nodes, are already the primary bottleneck for a P-BFS, consider the implications when using variable power.  When capping the power, the performance reduction of one node will affect the performance of all other nodes, since to complete each level-synchronous step at each level of the graph, each node must communicate with all other nodes. However, setting all active power caps to the same value for each node may not guarantee the same per-node progress, since each node's progress depends on the portion of the input graph it is given at each level.  As a result, each node's utilization and power will vary within each phase.  
%Inactive power capping imposes a similar constraint, since a parallel task cannot continue until every node completes its work.  
Thus, tasks may be finished with different speeds which creates synchronization barriers, and the overall progress is dictated by the slowest node to complete any phase. 

The presence of both inter-node dependencies and unpredictable performance across nodes (due to the differences of the input data they receive) poses challenges when power varies.  
%Most data-intensive applications, such as P-BFS, which consists of a series of synchronization barriers will exhibit such inter-node dependencies.  
Here optimizing embarrassingly parallel tasks, where each node operates at 100\% utilization and exhibits no inter-node dependencies, is a straightforward way of balancing the load, since any policy that uses all the available energy will yield the same performance.  
%We also note that the all-to-all barriers in a P-BFS are a particularly challenging type of communication pattern, since they require synchronization across every node. 
Thus, our energy-agile policies may be extended to a broader range of parallel computing applications with more complex communication patterns.  In fact, patterns that do not require all-to-all synchronization should permit more flexibility and greater performance improvements when optimizing for energy-agility. 

\subsection{\CWRED{Power Capping Model}}
\CWRED{In this section, we present an analytical model to characterize the foundational behavior of active and inactive power capping. In the next sections, we provide details on the energy management policies utilizing such power capping techniques. Since the actual performance varies depending on the system configuration, we perform a series of empirical evaluations of a real deployment in Section~\ref{sec:evaluation}.}

\CWRED{DVFS is a well-known CPU power management technique, which effectively reduces CPU power consumption by throttling processor frequency/voltage with the trade off of increasing the job execution time. 
There has been extensive research works focusing on the energy-performance trade-offs. While the conclusion depends on the specific configurations of computing nodes, i.e., the fraction of  idle power consumption and the fraction of CPU power to the total system power, it is generally agreed that large scale parallel applications are less sensitive to frequency scaling than sequential applications due to the inter-node communication overhead, which is not dependent on CPU frequency. For example, as estimated by Etinski et al.~\cite{ETINSKI2012579}, in their experiments with HPC MPI applications, ``20\% of frequency decrease nowadays leads to CPU power reduction of 37\%".}

\CWRED{The increased application runtime is determined by an application's sensitivity to frequency scaling, i.e., its CPU-boundness. Hsu and Kremer initially introduced the notion $\beta$, which represents the application's slowdown comparing to the CPU capacity reduction~\cite{Hsu-beta}. We utilize the processor dynamic power consumption equation as the basis of our power consumption estimation: $P_{CPU}=fCV^2+P_{static}$. We assume that $C$ and $P_{static}$ are fixed. Our dynamic power management policies adjusts the power capping within the range $f_{min}<f<f_{max}$. Adapting from the DVFS model of Meisner et al.~\cite{powernap}, the average power $P_{avg}$ is given by:}

\begin{equation}
P_{avg}=P_{max}(1-CPU_{frac}{(\frac{T(f)}{f_{max}})^3)},
\end{equation}

\CWRED{where $CPU_{frac}$ represents the fraction of CPU power comparing to overall system power. The application runtime $T(f)$ under CPU frequency $f$ and corresponding CPU power consumption reduction has been estimated estimated by Etinski et al.~\cite{ETINSKI2012579}:}

\begin{equation}
T(f)=pT(f_{max}) (\beta_{comp}(\frac{f_{max}}{f})+1)+(1-p)T(f_{max}),
\end{equation}
\CWRED{where $p$ represents the parallel efficiency which is determined by the application's sensitivity to frequency scaling. $\beta_{comp}$ represents the application slowdown comparing to CPU slowdown in the computing phase. Therefore, the corresponding CPU power reduction is calculated by:}
\begin{equation}
P_{saving}=\frac{1-\frac{P(f)}{P(f_{max})}}{CPU_{frac}}
\end{equation}

\CWRED{Given the estimated application runtime, we can compute the average power of each node as:}
\begin{equation}
P_{avg}=P_{inactive}T_{inactive}+P_{active}T_{active}+P_{comm}T_{comm}
\end{equation}

\CWRED{The resulting cluster-wide compute node energy consumption is:}

\begin{equation}
E=P_{total}T_{total}=\sum^N_{n=1}P^n_{avg}T_n(f),
\end{equation}
\CWRED{where $N$ represents the total number of nodes in cluster.}
















%This is the primary reason we focus on P-BFS, which consists of a series of simple all-to-all synchronization barriers, as opposed to an embarrassingly parallel task, where each node operates at 100\% utilization and thus exhibits no dependencies and predictable performance. 


%While we focus on one particular communication pattern here, we believe our energy-agile policies may be extended to more complex communication patterns, since they generally consist of a workflow of these barriers for different subsets of nodes, although this is future work.

%Since parallel tasks with more 

%Our policies are generalizable to parallel tasks with more complex communication patterns by 



% inter-node dependencies above within a parallel task


%% internode dependencies are what makes the problem hard. 




%Second, when using inactive power capping, all-to-all communication cannot complete until every completes the pair's  cross-node communication.  As a result, to minimize a P-BFS's runtime, an energy-agile system must transition nodes between the active and inactive states, within its available power constraints, to finish each all-to-all communication step.  For realistic transition latencies, the running time of each step, and hence the entire P-BFS, is a function of the number of node transitions, rather than the amount of computations per node or the network I/O between nodes.  Thus, an optimal P-BFS within the energy-agile model must incorporate a \emph{transition policy} that minimizes the total number of (synchronous) node transitions.



%
%While setting the active power caps 
%
%
%, where a fraction of nodes may be inactive at any given time.  
%
%In this case, all-to-all communication cannot complete until every ${N \choose 2}$ pair of nodes is concurrently active for long enough to complete the pair's  cross-node communication.  As a result, to minimize a P-BFS's runtime, an energy-agile system must transition nodes between the active and inactive states, within its available power constraints, to finish each all-to-all communication step.  For realistic transition latencies, the running time of each step, and hence the entire P-BFS, is a function of the number of node transitions, rather than the amount of computations per node or the network I/O between nodes.  Thus, an optimal P-BFS within the energy-agile model must incorporate a \emph{transition policy} that minimizes the total number of (synchronous) node transitions.
%




%More formally, for each period $t$, the sum of generated power $P_t$ and stored battery power in the previous period $B_{t-\tau}$ must be more than the actual power consumption in period $t$, $C_{t}$, i.e., $P_t+B_{t-\tau}-C_t\geq 0$. The generated power is a given signal depending on the power source, and varies with time. We aim to decide how to control the actual power usage over certain time interval based on how much energy is available. At each $t-\tau$ time interval, a power budget is given, we need to meet over the next time interval, $\tau$. The size of $\tau$ is based on the available energy storage, so that we can store the remaining energy from the previous time interval and then use the energy in the next interval. For smooth energy planning, we need an energy buffer that stores the energy from the previous time interval, and at the beginning of the next time interval we use it, so that we know how much energy we have.



\begin{comment}

The main goal of this paper is to propose and evaluate a set of algorithms to improve the power utilization efficiency in data centers, when limited amount of power budget is assigned to the cluster. In this section, we discuss the background information, the definition of the problem we are trying to solve, and then we present an interesting motivating example to show the necessity and effectiveness of power allocation strategies.

\subsection{Server Power Capping}
As a concept brought out in 2009, server power capping has been a commonly discussed technique in today's data center technologies. Power capping allocates fixed amount of power budget, allowing the system power consumption to stay below the defined power capping value. The power capping It can be broadly used to reduce the data center over-provisioning problem by setting a power limit below the peak power.

The traditional way of dealing with the insufficient available power situation is to turn off nodes or put servers to sleep mode, i.e., inactive power capping. However, with the help of active power capping, we are given higher flexibility control the actual power consumption of each server. As a trade-off, when power caps applies, the computational capacity of server reduces. Thus finding the best suitable power caps across the data center can be essential to optimize the overall performance.

\subsection{Demand Response Problem}
Another key factor for renewable energy powered data centers is demand response. Here in green data centers, one of the most concerned problem is how to make use of the available power budget to maintain the best possible data center performance.  More formally, for each period $t$, the sum of generated power $P_t$ and stored battery power in the previous period $B_{t-\tau}$ must be more than the actual power consumption in period $t$, $C_{t}$, i.e., $P_t+B_{t-\tau}-C_t\geq 0$. The generated power is a given signal depending on the power source, and varies with time. We aim to decide how to control the actual power usage over certain time interval based on how much energy is available. At each $t-\tau$ time interval, a power budget is given, we need to meet over the next time interval, $\tau$. The size of $\tau$ is based on the available energy storage, so that we can store the remaining energy from the previous time interval and then use the energy in the next interval. For smooth energy planning, we need an energy buffer that stores the energy from the previous time interval, and at the beginning of the next time interval we use it, so that we know how much energy we have.

\subsection{Parallel Computing and MPI}
\CW{we need to mention graph500 because it'll be used for the example}
For parallel applications, one of the most common used library is the Message Passing Interface (MPI). MPI is a standard for implementing distributed-memory parallel applications. The common architecture of MPI contains a single master node and a group of worker nodes. At the beginning of the application, the master node divide task into pieces, each of the worker nodes gets a portion of the overall computing problem. The worker nodes periodically synchronize with master node, and get new portion of the task. Usually users are not allowed to change the number of worker nodes after the program starts. The MPI-2.0 defined dynamic process management scheme, i.e., elasticity, however, currently the non-elastic MPI is still commonly used for most of the existing parallel applications.

Our system can be generally used for most parallel applications. To evaluate our system performance, we hire the commonly used Graph 500 benchmark~\cite{graph500}. Graph 500 benchmark was announced in 2010, and is designed to rate the high performance computers (HPC) when running large scale data intensive applications. In complement of other float point oriented benchmarks, such as High Performance Linpack, Graph 500 runs distributed breadth-first search in large graphs. It focuses more on the data communication throughout the system. In our system, we consider the cluster as a whole, thus Graph 500 fits our goal better. We use the Graph 500 implementation based on MPI. 

To provide a baseline for our comparison of power allocation strategies, we show the graph 500 runtime of different graph scales in Figure~\ref{fig:graph500-comparison}. This experiment runs on a 6 node mini cluster, and all the nodes runs under full power. The scale of Graph 500 is in terms of the logarithm base two of the number of vertices, e.g., the graph scale of 20 contains $2^{20}$ vertices.

\begin{figure}[!htb]
\centering
\includegraphics[width=1\linewidth, keepaspectratio=true]{figures/graph500-comparison.pdf}
\caption{The runtime of graph 500 when operating at full power}
\vspace{-.2in}
\label{fig:graph500-comparison}
\end{figure}

\subsection{Power Monitoring and Allocation Strategies}
Our main interest is to propose an efficient power allocation algorithm for renewable energy situation, where available energy changes over time, and are not always fully available to power the entire data center. From the service providers' point of view, a natural objective of a power allocation algorithm is to provide as much computational resources as possible while keep adaptive to the variation of available power resources. In this section, we start with a simple example on a setup with 4 servers, we aim to show how varying power allocation strategies can impact the actual performance of the cluster. As a starting point, we introduce two intuitive algorithms: \emph{greedy} and \emph{balanced}, then we introduce how we can achieve better power utilization by improving based on these two algorithms.
%Minimize the waste such that all of the nodes have the same power budget which is close to the peak power.

\subsubsection{``Wasted Power"}
Before introducing the algorithms, we define the term of ``Wasted Power". In this paper, we refer the wasted power as the server idle power, and/or consuming higher amount of power than its needed peak power consumption. For the servers we mentioned above, if current power reading is 100 W, only 20 W is counted towards the effective power usage, i.e., power consumed towards calculation, thus 80 W is wasted. Also the maximum effective power is 150 W, if we give power budget beyond this max power, the excessive power is wasted. For example, if the server is currently consuming 240 W power, then is wasting 96 W power.

On the other hand, typical cluster applications, like Graph 500, are required to exchange information with other nodes periodically, thus the slowest node decides the overall runtime of the application.

Therefore, here we would like to minimize the wasted energy while maintaining a balanced power distribution which is as close to the peak power as possible. 

\subsubsection{Motivating Example}
By using the native Dell Active Power Control (DAPC) application, which provides OS-independent power management, we are able to measure the fine-tuned power consumption of the server, and also control the available power cap for each server. The active power capping is achieved by assigning the servers synthetic CPU workload to limit the available CPU capacity.

By measuring the Graph 500 performance, we show how the active power capping can impact the application performance. For simplicity, we start with an example with 4 servers. Based on the measurements of Graph 500, the nominal peak power consumption without power capping is 150 W. We assume that the total available power is 400 W. 

We start from a simple example to illustrate our fundamental method. Here we use 3 power capping strategies, which are summarized in Figure~\ref{fig:powerCapping}. The Power monitoring and allocation strategies are discussed in detail in Section~\ref{sec:algorithm}.
\begin{itemize}
\item The first strategy gives the maximum number of servers the maximum possible power, i.e., allocate 150 W to first two servers, then allocate the rest of power to the third server (100 W). Put the rest of the servers to sleep. 
\item The second strategy allocate power equally to all the servers. In this case, none of the servers can get enough power to support maximum CPU speed.
\item For the third strategy, we integrates the above two, first calculate how many servers can be supported by available power, then equally distribute the power to those servers.
\end{itemize}

\begin{figure}[!htb]
\centering
\includegraphics[width=1\linewidth, keepaspectratio=true]{figures/power-capping.pdf}
\caption{The power capping stretegies}
\vspace{-.2in}
\label{fig:powerCapping}
\end{figure}

The above three cases are tested under different power scenarios. We run Graph 500 for 10 iterations for each data point. The available power for each server is represented on x axis, the average runtime of 10 iterations are shown on y axis. As can be observed, the third strategy shows us significantly less runtime then the other two. This clearly states that with same amount of available power, using appropriate power allocation strategy can make significant difference.

This behavior can be predicted by our wasted power calculation. For the first strategy, the wasted power for first 3 nodes is 240 W; for the second strategy, the wasted power is 320 W. For the third strategy, the wasted power is also 240 W, however, the power is more evenly distributed across the nodes, which fits the nature of most cluster applications: the total performance is usually decided by the slowest node.

\begin{figure}[!htb]
\centering
\includegraphics[width=1\linewidth, keepaspectratio=true]{figures/algo-comparison.pdf}
\caption{The power capping stretegies}
\vspace{-.2in}
\label{fig:algo-comparison}
\end{figure}

\end{comment}
