\documentclass[review]{elsarticle}
%\documentclass[conference, a4paper, 10pt, doublecolumn]{IEEEtran}
%\documentclass{sig-alternate}[11pt, letterpaper, doublecolumn]
%\documentclass{acm_proc_article-sp}
\usepackage{lineno,hyperref}
\modulolinenumbers[5]

\journal{Sustainable Computing}

\usepackage{times}
\usepackage{url}
\usepackage{multirow}
\usepackage{comment}
\usepackage{epsfig}
\usepackage{epstopdf}
\usepackage{balance}
\usepackage{subfigure}
\usepackage{color}
\usepackage{wrapfig}
\usepackage[latin9]{inputenc}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{fancyhdr}

%\usepackage[boxed,commentsnumbered,ruled,vlined,linesnumbered]{algorithm2e}
\usepackage[algo2e]{algorithm2e} 
%\usepackage{llncsdoc}
%\usepackage{graphicx}

%\PassOptionsToPackage{bookmarks=false}{hyperref}
%\renewcommand{\UrlBreaks}{\do\/\do\a\do\b\do\c\do\d\do\e\do\f\do\g\do\h\do\i\do\j\do\k\do\l\do\m\do\n\do\o\do\p\do\q\do\r\do\s\do\t\do\u\do\v\do\w\do\x\do\y\d\o\z\do\A\do\B\do\C\do\D\do\E\do\F\do\G\do\H\do\I\do\J\do\K\do\L\do\M\do\N\do\O\do\P\do\Q\do\R\do\S\do\T\do\U\do\V\do\W\do\X\do\Y\do\Z}
%\usepackage{breakurl}

\usepackage{algorithm} 
\usepackage{algorithmic}
\usepackage{multirow} 
\usepackage{amsmath}
\usepackage{xcolor}
\usepackage[normalem]{ulem} % strikethrough comment sout

\renewcommand{\algorithmicrequire}{\textbf{Input:}}
\renewcommand\algorithmicensure {\textbf{Output:} }

\newfont{\mycrnotice}{ptmr8t at 7pt}
\newfont{\myconfname}{ptmri8t at 7pt}
\let\crnotice\mycrnotice%
\let\confname\myconfname%

\newcommand\CW[1]{\textcolor{black}{#1}}
\newcommand\CWRED[1]{\textcolor{red}{#1}}
\newcommand\DI[1]{\textcolor{green}{#1}}
\newcommand\MZ[1]{\textcolor{blue}{#1}}

\hyphenpenalty=1200
\tolerance=400

\def\Section {\S}

\newcommand{\squishlist}{
 \begin{list}{$\bullet$}
  { \setlength{\itemsep}{0pt}
     \setlength{\parsep}{3pt}
     \setlength{\topsep}{3pt}
     \setlength{\partopsep}{0pt}
     \setlength{\leftmargin}{1.5em}
     \setlength{\labelwidth}{1em}
     \setlength{\labelsep}{0.5em} } }

\newcommand{\squishlisttwo}{
 \begin{list}{$\bullet$}
  { \setlength{\itemsep}{0pt}
     \setlength{\parsep}{0pt}
    \setlength{\topsep}{0pt}
    \setlength{\partopsep}{0pt}
    \setlength{\leftmargin}{2em}
    \setlength{\labelwidth}{1.5em}
    \setlength{\labelsep}{0.5em} } }

\newcommand{\squishend}{
  \end{list}  }

%\makeatletter
%\let\@copyrightspace\relax
%\makeatother

\sloppy

\pagenumbering{arabic}
\begin{document}
\begin{frontmatter}

\title{Energy-Agile Design for Parallel HPC Applications\tnoteref{t1}}
\tnotetext[t1]{Part of this work has been published in Proceedings of IGSC:  IEEE International Green and Sustainable Computing Conference (Wang et al. 2015.~\cite{igsc15})}

\author[rvt1]{Cong Wang\corref{cor1}\corref{cor2}}
\ead{cwang@renci.org}
\author[rvt2]{Michael Zink}
\ead{zink@ecs.umass.edu}
\author[rvt2]{David Irwin}
\ead{irwin@ecs.umass.edu}

\cortext[cor1]{Corresponding author}
\cortext[cor2]{This work was performed while the author was with University of Massachusetts Amherst}
\address[rvt1]{RENCI, Chapel Hill, NC 27517, USA}
\address[rvt2]{University of Massachusetts, Amherst, MA 01003, USA}
%\author{Cong Wang, Michael Zink and David Irwin}
%\address{Department of Electrical and Computer Engineering\\
%       University of Massachusetts Amherst\\
%       \{cwang, zink, irwin\}@ecs.umass.edu\\}

%\author{
%Cong Wang, Michael Zink and David Irwin\\
%       Department of Electrical and Computer Engineering\\
%       University of Massachusetts Amherst\\
%       \{cwang, zink, irwin\}@ecs.umass.edu\\
%}

%\IEEEoverridecommandlockouts
%\IEEEpubid{\makebox[\columnwidth][c]{978-1-5090-0172-9/15/\$31.00~\copyright2015 IEEE \hfill} \hspace{\columnsep}\makebox[\columnwidth]{ }} 


%\maketitle

\begin{abstract} 
While there has been significant prior research on optimizing the energy-efficiency of parallel applications, there has been much less on optimizing them for green energy sources, which expose rapid changes in power's availability (or cost) due to the use of local renewable energy (or utility demand response programs). In this article, we present energy management policies that utilize active and inactive power capping to maximize the performance of rigid and elastic parallel tasks when subject to variable power constraints from green energy sources. We implement our policies on a real cloud testbed, and evaluate the performance with three different parallel applications. Our results demonstrate the importance of designing green energy management policies with variable power. For example, we show that a reference supercomputer benchmark application requires 17\% more time and 9\% more energy to complete when power varies based on real-time electricity prices versus when power is unlimited at a fixed price.  However, since the real-time spot market prices are lower than fixed prices, the total electricity cost of our best energy management policy when using real-time prices is 67\% less than when using fixed prices. 
\end{abstract}

\begin{keyword}
Optimization, Green Computing, Parallel Application, High Performance Computing, Renewable Energy
\end{keyword}

\end{frontmatter}

\linenumbers

\section{Introduction}
\label{sec:intro}
\input{Introduction2}

\section{Background}
\label{sec:background}
\input{background}

\section{Dynamic Energy Management Policies}
\label{sec:algorithm}
\input{algorithm}

\section{Implementation}
\label{sec:implementation}
\input{implementation}

\section{Performance Evaluation}
\label{sec:evaluation}
\input{evaluation2}

\section{Related Work}
\label{sec:related}
\input{related}

\vspace{-0.2cm}
\section{Conclusion}
\label{sec:conc}
In this article, we investigate energy management policies for parallel computing applications that run on green energy sources.
%, especially the ones that exhibit inter-dependencies among nodes that energy management policies can optimize the runtime and energy consumption.  
We propose a variety of energy management policies that make use of both active and inactive power capping to maximize parallel application performance when running on power resources with variable constraints. Our policies include both static and dynamic power allocation variants that can be applied to both elastic and rigid (non-elastic) parallel jobs.

We implement our energy management policies on two different clusters in a real testbed environment for three parallel applications. We evaluate the effectiveness of proposed policies based on real solar, wind, and spot-price-based power signals. Our results demonstrate the importance and effectiveness of designing for variable power. We show that the parallel applications require up to 17\% more time and 9\% more energy to complete when power varies based on real-time electricity prices versus when power is unlimited at a fixed price.  However, since real-time prices are lower than fixed prices, the total electricity cost of our energy-agile policy with real-time spot prices is 67\% less than when using fixed prices. 

As future work, we intend to further improve the performance and expand the utilization of energy management techniques. E.g., by deciding energy availability based on the price, application runtime can be further reduced by combining renewable energy resources and minimal usage of energy from the grid. Also our energy management policies can be extended for increasing the resilience of renewable energy powered data centers and handling grid failure, e.g., in case of hazardous weather or intermittent grid brownouts.

\section{Acknowledgement}
\label{sec:ack}
This work was funded by the National Science Foundation under grants \#1419199, \#1331572, and \#1422245. Any opinions, findings, or conclusions in this article are those of the authors and do not necessarily reflect the views of the sponsor.

\bibliographystyle{abbrv}
\bibliography{refs}

\end{document}

%Possible journals: TPDS - IEEE Transactions on Parallel and Distributed Systems ,JPDC - Journal of Parallel and Distributed Computing, DC - Distributed Computing, PC - Parallel Computing
