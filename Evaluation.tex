%\CW{Will add a section introducing CloudLab and a section to introduce three applications. The graph500 is considered as the main evaluated application, the other two are used as interference applications. So we show the graph500 runtime and power consumption under the circumstances of single application, 2 applications, 3 applications, respectively}

Our experiments utilize power signals from real solar and wind deployments and traces of market power prices. We collect the solar and wind power traces from our own solar panel and wind turbine deployments. For each of these traces, we select one typical day with average power readings every minute.\footnote{For solar power, we selected an early Fall day, September 28, 2014 from 12am to 11:59pm. For wind power, we selected a typical spring day, April 26, 2014 from 12am to 11:59pm.} The power signal for the solar and wind traces is shown in Figure~\ref{fig:solar-energy} and Figure~\ref{fig:wind-energy}. For the energy price trace, we use the five-minute spot price from the New England Independent System Operator (ISO) on October 5, 2013 from 12am to 11:59pm. Since both the solar and wind energy traces have blank period with little available energy, we cut the energy traces with the most effective period. Also in order to ensure fairness, we normalized all three power resources with the same average power. We assume our system has a fixed power budget for each 20-second period, such that during a low price period, we purchase more power, and during a high price period, we purchase less power. This policy results in the power signal shown in Figure~\ref{fig:energy-n-price}, which is derived from the variations in price. Given the traces above, our evaluation first benchmarks the power consumption of our prototype energy-agile cluster, and then evaluates the policies from Section~\ref{sec:algorithm} using the three power signal traces. We use three reference parallel applications on the CloudLab cluster with 65 nodes. 

\subsection{Energy-Agile Cluster Profiling}
\label{subsec:eponlocal}
In this section we show the profile of power utilization in terms of Graph500 scale and CPU utilization on each server. With the profile data, we will be able to emulate the power capping mechanisms for the whole cluster.

We first show the power consumption of running Graph500 on our prototype cluster with 65 nodes - 1 master node and 64 worker nodes. Figure~\ref{fig:scale-vs-power} quantifies the average power usage when executing Graph500 on different graph scales. Here, we measure the average power usage across all 64 worker nodes, where the error bars represent the standard deviation. As shown in Figure~\ref{fig:scale-vs-power}, the power consumption does not change significantly for graph scales between the range of 14 and 24. However, after a scale of 24, power consumption increases significantly. In addition, the standard deviation becomes larger when increasing the graph scale, which indicates that the difference of power consumption among the worker nodes grows with the graph size. Recall that energy-agility becomes more critical as the variance in power usage and utilization increases. Thus, this result shows that as graph size grows, the opportunity of optimizing the power distribution increases. In the remaining experiments, we use a graph scale of 25, which incurs moderate power usage without significantly increasing power's standard deviation across nodes. Our choice is conservative, since the greater the standard deviation, the more opportunity for gains by shifting power between nodes.

\begin{figure}[!t]
\centering
\includegraphics[width=0.85\linewidth, keepaspectratio=true]{figures/scale-vs-power.pdf}
\vspace{-0.85cm}
\caption{Variation in power consumption when running Graph500 for different size graphs. The larger the graph, the greater the runtime and standard deviation in runtime.}
\vspace{-0.45cm}
\label{fig:scale-vs-power}
\end{figure}

To determine the relationship between CPU load and power consumption, we next perform an CPU stress experiment on single node. We gradually increase the CPU load, and record the corresponding power consumption.  We ran the experiment ten times for each level of CPU load. Figure~\ref{fig:cpu-vs-power} shows the average and standard deviation of power consumption for ten runs. As expected, the power consumption increases near linearly with CPU load, which verifies our hypothesis: CPU load is a reasonable proxy for dynamic power usage. This graph also shows that at 0\% utilization in our prototype cluster, each node uses near 50W, which is about 40\% of the peak 125W power usage at 100\% utilization. 

We use the results in Figures~\ref{fig:scale-vs-power} and~\ref{fig:cpu-vs-power} to determine the CPU utilization to power usage mapping. Given this CPU-to-power mapping, for the remainder of this paper, we use node's CPU utilization as a proxy for power consumption and then cap maximum available CPU instead of power.  For example, based on Figure~\ref{fig:cpu-vs-power}, in order to set the power cap of a node to 75W, we can limit the maximum CPU utilization of the node to 30\%.

\begin{figure}[!t]
\centering
\includegraphics[width=0.85\linewidth, keepaspectratio=true]{figures/cpu-vs-power.pdf}
\vspace{-0.85cm}
\caption{Power usage at different CPU load levels in our prototype six node energy-agile cluster.}
\vspace{-0.2cm}
\label{fig:cpu-vs-power}
\end{figure}

\subsection{Energy-Agile Policy Performance}

We perform our experiments on 65 physical nodes in CloudLab testbed, with one master node and 64 worker nodes. In this section, we show the performance of our energy-agile policies on Graph500 when running together with the other two applications, WRF and Jacobi. These experiments utilizing realistic power signal showing in Section~\ref{subsec:eponlocal}. We begin by analyzing the performance of both the open-loop and closed-loop balanced policy for rigid (non-elastic) parallel tasks, then examine the performance of our agile policy in the case of a more flexible elastic application. 

\subsubsection{Rigid Energy-agile Performance}

Figure~\ref{fig:nonelastic-runtime} shows the runtime of Graph500 with solar, wind, and price-based power traces for both our open-loop (static) and closed-loop (dynamic) balanced policies that only employ active power capping. As shown in Figure~\ref{fig:nonelastic-runtime}, our closed-loop balanced policy reduces the running time of the Graph500 benchmark by 19\% for our solar power trace, by 17\% for our wind power trace, and by 14\% for our price-based power trace, respectively, compared to an open-loop balanced policy.  This shows that adjusting power caps at fine-grained intervals can effectively shift power to the nodes that are able to make use of it. This result is significant, as it demonstrates that our closed-loop policy is able to take advantage of the variations in inter-node power usage as shown in Figure 2 by adjusting node power caps at fine-grained intervals, e.g., once a second, which reduces the running time of the Graph500 benchmark.
\\
\noindent {\bf Result:} \emph{Closed-loop control that continuously reallocates power to the nodes that can use it the most improves the performance of Graph500 by 14-19\% in our experiments.}
\\
\begin{figure}[!t]
\centering
\includegraphics[width=0.7\linewidth, keepaspectratio=true]{figures/s-v-d-nonelastic-runtime.pdf}
\vspace{-0.45cm}
\caption{Runtime of rigid Graph500 for solar, wind and price-based power signals using both the closed-loop (dynamic) policy and open-loop (static) balanced policy that employ active power capping.}
\vspace{-0.45cm}
\label{fig:nonelastic-runtime}
\end{figure}

\begin{figure}[!t]
\centering
\includegraphics[width=0.7\linewidth, keepaspectratio=true]{figures/s-v-d-nonelastic-power.pdf}
\vspace{-0.45cm}
\caption{Energy consumption of rigid Graph500 for solar, wind and price-based power signals using both the closed-loop (dynamic) policy and open-loop (static) balanced policy that employ active power capping.}
\vspace{-0.45cm}
\label{fig:nonelastic-power}
\end{figure}

\subsubsection{Elastic Energy-Agile Performance}

Our elastic energy-agile policy is able to activate and deactivate subsets of nodes. However, as discussed earlier, activating and deactivating nodes imposes a long transition time and associated overhead, which may actually reduce overall performance. One way to limit the number of transitions is to increase the interval $\tau$ over which power is known and stable, since our policies only transition nodes between the active and inactive state at the beginning of each interval. For our experiments, we assume a transition time of 10 seconds to transition from the active to inactive state and back again, which is typical for transitions to and from ACPI's S3 state.

\noindent {\bf Energy Storage}. As mentioned in Section~\ref{sec:background}, the capacity of the energy storage, i.e., battery, used to buffer energy, determines the length of $\tau$.  In Figure~\ref{fig:t-vs-tau}, we show how Graph500's runtime varies for different values of $\tau$.  As shown in the results, when $\tau$ increases, application runtime decreases, due to fewer number of transitions. For example, for the wind power trace, the runtime decreases by 7$\times$ (from over 70,000 seconds to less than 10,000 seconds) when $\tau$ increases from 5 to 100 seconds. These results show that the cluster performance greatly depends on the energy buffer size, however, a relatively small buffer size ($<150$ seconds) can still provide good performance, since runtime decreases to near the minimum after $\tau=150$ seconds. 

For the rest of the experiments, we choose a moderate energy buffer $\tau$ of $60$ seconds, since a $60$-second energy buffer does not require a large battery size, while it can significantly reduce the application runtime compared to a smaller energy buffer size.  Thus, out of every $60$ second interval, we potentially use $1/6$th of the interval to transition a node between the active and inactive states. 
\\
\noindent {\bf Result:} \emph{A larger energy buffer and longer $\tau$ improves performance up to a point.  In our experiments a buffer greater $\tau=150$s has little impact on performance.}
\\
\begin{figure}[!t]
\centering
\includegraphics[width=0.85\linewidth, keepaspectratio=true]{figures/t-vs-tau-all.pdf}
\vspace{-0.65cm}
\caption{Runtime of elastic Graph500 with different storage capacities ($\tau$).}
\vspace{-0.35cm}
\label{fig:t-vs-tau}
\end{figure}

\noindent {\bf Transition Time}. We also plot the effect on performance of varying the transition time, i.e., the time required for servers to go from active to inactive, in Figure~\ref{fig:transition-time}.  As shown in Figure~\ref{fig:transition-time}, the runtime increases nearly linearly when the transition time is below 60 seconds, but after that, the application runtime increases quadratically as the transition time increases. 
\\
\noindent {\bf Result:} \emph{Shorter transition times ($<$60 seconds) are important for optimizing energy-agility.}
\\

\noindent {\bf Open- versus Closed-loop}. In Figure~\ref{fig:algorithm-runtime}, we show the overall runtime comparison for our elastic agile policy when using both a balanced open-loop (static) policy within each interval and when using a balanced closed-loop (dynamic) policy. We gradually increase the number of worker nodes from 2 to 64, then record the total runtime of Graph500. The measurement was repeated three times with the case of single application, two applications and three applications, respectively. The error bars represent standard deviation across all the nodes. As shown in the figures, the runtime of agile-static (using an open-loop policy for active power capping) is always longer than agile-dynamic (using a closed-loop policy for active power capping).
% The runtime difference becomes smaller as the number of nodes increases; this occurs because both policies are approaching the limit of Graph500's runtime.

As shown in Figure~\ref{fig:algorithm-runtime-price}, the runtime for both agile-static and agile-dynamic is lower for the case of the price-based power trace in comparison to the solar and wind traces. This is because there are fewer power variations and higher overall power availability in the price-based signal. By contrast, the solar power signal shows slightly longer runtime than for the price signal (see Figure~\ref{fig:algorithm-runtime-solar}).  One reason for this difference is that solar power availability gradually increases and then decreases, which provides more time for an energy-agile policy to adjust the power allocation across the nodes. Figure~\ref{fig:algorithm-runtime-wind} shows that the wind-based power trace results in the longest runtime and highest variation. The long running time is consistent with the high variation, and low availability of wind power.

%% Shouldn't we say what the average power is in each trace? Clearly lower power, gives less performance. 

\begin{figure}[!t]
\centering
\includegraphics[width=0.85\linewidth, keepaspectratio=true]{figures/transition-time.pdf}
\vspace{-0.65cm}
\caption{The runtime of elastic Graph500 as a function of the inactive transition time.}
\vspace{-0.35cm}
\label{fig:transition-time}
\end{figure}

In all three figures, the dynamic, closed-loop agile policy results in significantly less running time than the static, open-loop policy due to the freedom of changing power allocations. The runtime difference is greater when fewer worker nodes are involved in the application, and decreases when the number of worker nodes increases.  In addition, as the number of worker nodes increases, both the static, open-loop and dynamic, closed-loop policies converge to the limit of the cluster's capability.
\\
\noindent {\bf Result:} \emph{Our elastic agile policy benefits from using closed-loop control within each interval $\tau$. The benefit increases with the number of nodes in the cluster.}
\\
\begin{figure*}[!t]
\centering
\subfigure[]{
\includegraphics[width=0.3\linewidth, keepaspectratio=true]{figures/algorithm-runtime-solar.pdf}
\label{fig:algorithm-runtime-solar}
 }
 \subfigure[]{
\includegraphics[width=0.3\linewidth, keepaspectratio=true]{figures/algorithm-runtime-wind.pdf}
\label{fig:algorithm-runtime-wind}
 }
 \subfigure[]{
\includegraphics[width=0.3\linewidth, keepaspectratio=true]{figures/algorithm-runtime-price.pdf}
\label{fig:algorithm-runtime-price}
 }
\centering
\vspace{-0.1cm}
\caption{The runtime of our elastic variant of Graph500 when using a solar power signal (\ref{fig:algorithm-runtime-solar}), wind power signal (\ref{fig:algorithm-runtime-wind}), and a power signal based on electricity price data (\ref{fig:algorithm-runtime-price}).}
\vspace{-0.3cm}
\label{fig:algorithm-runtime}
\end{figure*}

\begin{figure*}[!t]
\centering
\subfigure[]{
\includegraphics[width=0.3\linewidth, keepaspectratio=true]{figures/algorithm-power-solar.pdf}
\label{fig:algorithm-power-solar}
 }
 \subfigure[]{
\includegraphics[width=0.3\linewidth, keepaspectratio=true]{figures/algorithm-power-wind.pdf}
\label{fig:algorithm-power-wind}
 }
 \subfigure[]{
\includegraphics[width=0.3\linewidth, keepaspectratio=true]{figures/algorithm-power-price.pdf}
\label{fig:algorithm-power-price}
 }
\centering
\vspace{-0.1cm}
\caption{The energy consumption of our elastic variant of Graph500 when using a solar power signal (\ref{fig:algorithm-power-solar}), wind power signal (\ref{fig:algorithm-power-wind}), and a power signal based on electricity price data (\ref{fig:algorithm-power-price}).}
\vspace{-0.3cm}
\label{fig:algorithm-power}
\end{figure*}

\noindent {\bf Elastic Policy Comparison.} Figure~\ref{fig:static-vs-dynamic} shows the runtime of three policies: balanced, greedy, and agile.  The balanced policy is not elastic, while the greedy and agile policy are elastic, as described in Section 3.   As shown in the figures, for all the traces and energy-agile policies, dynamically re-distributing power using active power capping significantly reduces runtime, by up to 26\% for greedy, 55\% for balanced, and 31\% for agile.  Another observation is that, with the balanced policy, the dynamic, closed-loop variant has better performance than the static, open-loop variant when the power signal has more fluctuations, e.g., for wind power traces. This occurs because the power fluctuation gives the dynamic, closed-loop balanced policy more room to correct the balance in power among the cluster nodes. These results indicate that continuously re-allocating power is especially suitable for data centers powered by renewable energy, where available energy varies by time.

\CW{ explain the effect of adding more applications}

In addition, comparing the elastic tasks in Figure~\ref{fig:s-v-d-solar} - Figure~\ref{fig:s-v-d-price} with the rigid (non-elastic) tasks in Figure~\ref{fig:nonelastic-runtime} shows that elastic applications that also employ a closed-loop dynamic policy within each interval achieve 41\% less runtime for solar power traces, 36\% less runtime for wind power traces, and 33\% less runtime for our price-based power traces. This also indicates the importance of elasticity in energy-agile design. 

Finally, in Figure~\ref{fig:static-vs-dynamic-power}, we show the resulting energy consumption for the solar, wind and price-based signals. Here, the energy consumption is the product of the average power usage and the application's runtime.  As the figure shows, the dynamic closed-loop policy variant reduces energy usage for each of the three elastic policies: by 28\% for the greedy policy, by 64\% for the balanced policy, and by 41\% for the agile policy.   
\\
\noindent {\bf Result:} \emph{Energy-agile policies that permit elasticity significantly outperform, between 33-41\% less runtime and 28\%-64\% less energy usage, policies that do not.}
\\

%%We compare these results with Figure~\ref{fig:fixed-full-runtime} and Figure~\ref{fig:fixed-full-power}, which show the runtime and energy consumption, respectively, of Graph500 when it runs \emph{i)} with unlimited power and \emph{ii)} with a fixed power cap equal to the average power in each case. 

\noindent {\bf Impact of Variability.}   We also compare performance when power is variable with the performance when it is not variable and unlimited, i.e., nodes are free to use as much power as necessary at any time, and when it is not variable and capped, i.e., nodes must adhere to a static power cap.  Figure~\ref{fig:fixed-full-runtime} and Figure~\ref{fig:fixed-full-power} show the runtime and energy consumption, respectively, of Graph500 when it runs \emph{i)} with unlimited power and \emph{ii)} with a fixed power cap equal to the average power.

As expected, the greedy, balanced, and agile policies using variable power (in Figure~\ref{fig:static-vs-dynamic} and Figure~\ref{fig:static-vs-dynamic-power}) take longer to complete and consume more energy than with unlimited power or with a stable power cap.  In particular, comparing to unlimited power, the greedy policy takes up to 51\% longer and uses 43\% more energy, the balanced policy takes up to 83\% longer and uses up to 81\% more energy, and the agile policy takes up to 33\% longer and uses up to 21\% more energy.  Likewise, comparing to a stable power cap, the greedy policy takes up to 36\% longer and uses up to 39\% more energy, the balanced policy takes up to 78\% longer and uses up to 75\% more energy, and the agile policy takes up to 12\% longer and uses up to 8\% more energy.  

The results above both illustrate that energy-agile design is important and address its limits.  Since adapting to variable power introduces overheads that might cause an application to run longer than necessary, it typically uses more energy overall than when using unlimited or stable power. The goal of energy-agile design is to limit this additional energy; our results show that our agile policy uses only 21\% more energy when using unlimited power and only 8\% more energy when using stable power. These comparisons are based on the worst case scenario, i.e., we compare the unlimited and stable power policies with the longest running time and highest energy usage among the three power varying cases.

%When comparing the performance when using unlimited power and static power, the greedy, balanced, and agile policies (in Figure~\ref{fig:static-vs-dynamic} and Figure~\ref{fig:static-vs-dynamic-power}), our results show that Graph500 takes shorter time to run when utilizing full power (39\% for greedy, 80\% for balanced and 25\% for agile), and thus resulting in less energy consumption (31\% for greedy, 78\% for balanced and 14\% for agile).  This 

% In case 2, with fixed power cap of the average of full power, the runtime and power consumption are very close to the closed-loop agile policies. The runtime is 21\%, 72\% and 4\% less than greedy, balanced and agile, respectively; while the consumed energy is 23\%, 74\%, and 2\% less than greedy, balanced and agile, respectively. The full power case results in less runtime and energy consumption, however, the disadvantage of full power case is clear: the full power case cannot take into account the power availability, price variation and green energy.

Based on the energy consumption data and energy prices in Figure~\ref{fig:examples}, we are able to calculate the overall energy cost for our policies with unlimited power and stable  power, as well as using our price-based power signal with the greedy, balanced, and agile policies. The results in Table~\ref{tab:power-cost} show that although using unlimited power yields the shortest runtime and lowest energy consumption, it costs 41\% more than using the greedy policy and 71\% more than using the agile policy. This price advantage occurs because using unlimited power or stable power does not react to changes in price. 
\\
\noindent {\bf Result:} \emph{Using variable power causes applications to run longer, which increases their overall energy use.  In our experiments, Graph500 requires 33\% more time and 21\% more energy to complete when power varies based on real-time electricity prices versus when power is unlimited at a fixed price.  However, since real-time prices are lower than fixed prices, the total electricity cost of our best energy-agile policy when using real-time prices is 71\% less than when using fixed prices.}
\\
% We are not showing the renewable energy cases because it is difficult to accurately calculate the real cost on renewable energy resources. As a proof of concept, the energy cost is calculated for running a relatively small scale application for only once. However, when running large scale, long running applications, the difference of energy cost can grow into a significantly large number.

\noindent {\bf Overhead Power.} Finally, Table~\ref{tab:power-overhead} summarizes the overhead power for each of the elastic policies.  Recall that we define overhead power as essentially the idle power of a node. Here, we represent overhead power as a percentage of the total energy consumed.  For rigid tasks, the dynamic policies can reduce the overhead power by 15\%, 12\%, and 9\% for solar, wind and price-based signals, respectively. In the case of elastic applications, the average reduction in overhead power is 18\%, 23\%, and 15\%, respectively.  The elastic tasks lower the overhead power more because they deactivate nodes completely to eliminate overhead power.  If we specifically examine wind energy, in the case of the static rigid balanced policy, we classify 75\% of the consumed power as overhead with only 25\% of the total available power going towards computation.  However, when applying the agile policy to elastic applications, only 35.6\% of total power was consumed as overhead.  Such significant reductions in overhead power mean that more energy is devoted to meaningful computation load, which leads to shorter runtimes and less energy waste.
\\
\noindent {\bf Result:} \emph{Reducing overhead power increases energy-efficiency.  Dynamic closed-loop policies that use active power capping reduce overhead power by allocating power to its most efficient use, while policies that permit elasticity further reduce it by activating and deactivating nodes.}
\\
\begin{figure*}[!t]
\centering
\subfigure[]{
\includegraphics[width=0.3\linewidth, keepaspectratio=true]{figures/s-v-d-solar.pdf}
\label{fig:s-v-d-solar}
 }
 \subfigure[]{
\includegraphics[width=0.3\linewidth, keepaspectratio=true]{figures/s-v-d-wind.pdf}
\label{fig:s-v-d-wind}
 }
 \subfigure[]{
\includegraphics[width=0.3\linewidth, keepaspectratio=true]{figures/s-v-d-price.pdf}
\label{fig:s-v-d-price}
 }
  \centering
  \vspace{-0.1cm}
\caption{Runtime of elastic variant of Graph500 for the greedy, balanced and agile policies for our solar (\ref{fig:s-v-d-solar}), wind (\ref{fig:s-v-d-wind}), and price-based power signals (\ref{fig:s-v-d-price}).}
\vspace{-0.2cm}
\label{fig:static-vs-dynamic}
\end{figure*}

\begin{figure*}[!t]
\centering
\subfigure[]{
\includegraphics[width=0.3\linewidth, keepaspectratio=true]{figures/s-v-d-solar-power.pdf}
\label{fig:s-v-d-solar-power}
 }
 \subfigure[]{
\includegraphics[width=0.3\linewidth, keepaspectratio=true]{figures/s-v-d-wind-power.pdf}
\label{fig:s-v-d-wind-power}
 }
 \subfigure[]{
\includegraphics[width=0.3\linewidth, keepaspectratio=true]{figures/s-v-d-price-power.pdf}
\label{fig:s-v-d-price-power}
 }
  \centering
  \vspace{-0.1cm}
\caption{Total energy consumption of elastic variant of Graph500 for the greedy, balanced and agile policies for our solar (\ref{fig:s-v-d-solar-power}), wind (\ref{fig:s-v-d-wind-power}), and price-based power signals (\ref{fig:s-v-d-price-power}).}
\vspace{-0.2cm}
\label{fig:static-vs-dynamic-power}
\end{figure*}

\begin{table}[!t]
%\newcommand{\tabincell}[2]{\begin{tabular}{@{}#1@{}}#2\end{tabular}}
\centering
\begin{tabular}{ c | c } 
\hline
Power Policy & Energy Cost (cents)\\
\hline
Full Power & 16.12\\ 
Stable Power & 21.04\\ 
Price Greedy & 11.02\\ 
Price Balanced & 38.47\\ 
Price Agile & 8.94\\ \hline
\end{tabular}
\caption{Comparison of energy cost for non-renewable powered cluster.}
\label{tab:power-cost}
\end{table}

%The last two figures are in conclusion.