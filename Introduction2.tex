Energy consumption is widely regarded as the primary design constraint for scaling up the capacity of High Performance Computing (HPC) platforms, mainly due to the cost and carbon footprint of powering and cooling these systems~\cite{challenges}.  To illustrate the magnitude of this constraint, recent estimates project that, if historical trends continue, the power requirements of an exascale platform in 2020 would be 200MW~\cite{challenges} with an annual electricity bill greater than \$2.5B~\cite{subcommittee-report} primarily composed of ``dirty'' energy sources.  These costs are unsustainable for even the highest-value applications.  To reduce costs and carbon emissions, researchers continue to focus heavily on improving the energy-efficiency, i.e., the amount of computation done per joule, as evidenced by the ``green'' variants of the TOP500~\cite{top500} and Graph500~\cite{graph500} rankings.  Even so, reaching the current exascale design target of 20-40MW by 2020 will require an order of magnitude further improvement in energy-efficiency~\cite{exascale-report}.  Achieving such improvements appears unrealistic in the near-term~\cite{simon-presentation}.  

Thus, in this article, we target a promising new direction for reducing energy costs and carbon emissions that focuses on improving the ability to rapidly and efficiently adapt parallel applications to dynamic changes in available power or cost.   Our work envisions a tight coupling between the electric grid and large-scale computing platforms, enabling them to work in concert to efficiently balance electricity's supply and demand.  Today's grid is grossly inefficient, while utilities are beginning to experiment with demand response programs (as described below), in most cases, utilities still balance supply and demand largely by only regulating supply, while granting consumers the freedom to \emph{use as much power as they want, whenever they want.}   This freedom imposes a steep price along multiple dimensions, resulting in wasted capital investments, high operational costs, and limited renewable penetration.   

%From the reviewer of igsc: “Today’s grid is grossly inefficient, primarily because utilities balance supply and demand by only regulating supply, while granting consumers the freedom to use as much power as they want, whenever they want.”
%This is not accurate statement. I am aware of several markets where energy companies are installing switches to regulate AC at the customer home to soothe the demand spikes.

The grid's inefficiencies have motivated recent ``smart'' grid initiatives to reduce peak demands and better handle intermittent renewables using demand-side management, which balances supply and demand, in part, by regulating electrical loads' energy usage, e.g., via real-time pricing or demand response.  These initiatives offer consumers the potential for significantly lower costs, while also enabling consumers to use more green energy from renewable sources.  As one example, Figure~\ref{fig:examples} plots electricity's real-time price (in New England's five-minute spot market) on October 5, 2013 from 12am to 11:59pm to highlight that it varies dramatically even over short time-scales.  These fluctuations enable consumers to reduce costs by increasing energy usage when prices drop, and decreasing it when prices rise.  In addition, the ability to adapt to power fluctuations also enables platforms to reduce their carbon emissions by increasing their reliance on local renewable energy sources, which generate energy intermittently based on changing environmental conditions.  
%\IEEEpubidadjcol

As outlined below, HPC platforms are well-suited to exploit such demand-side optimizations for four reasons. 

\squishlisttwo

\item {\bf Sophisticated Power Management}. HPC platforms already include advanced, remotely programmable power management mechanisms, making them capable of rapidly and precisely controling their energy usage over a wide dynamic range.

\item {\bf Delay-Tolerant Workloads}. Many HPC workloads consist of non-interactive batch jobs that are tolerant to delays in execution, providing them the flexibility to adjust their energy usage over time~\cite{6195508}. The price elasticity of demand is higher for these workloads than many household and industrial loads, which are often interactive and not highly responsive to price fluctuations.  

%\item {\bf Large Energy Consumers}.  The power requirements of future exascale-class HPC platforms ($>$20MW) will position them as some of largest industrial energy consumers with the highest electricity costs. As a result, they will have the most to gain from adapting their energy usage in response to changing grid conditions.

\item {\bf Rapid Growth Sector}.  Despite energy-efficiency improvements, the power demands of HPC and data centers continue to rise, increasing by an estimated 56\% from 2005-2010 and accounting for 1.7-2.2\% of U.S. electricity usage~\cite{koomey11}, with usage expected to double every five years~\cite{epa-report}.   
%If current trends continue, these platforms will comprise 20\% of U.S. electricity by 2030~\cite{presentation}.  
Thus, regulating their demand holds significant potential to improve grid efficiency, and reduce costs.

\squishend

\begin{figure}[t] 
\centering
\includegraphics[width=0.55\linewidth, keepaspectratio=true]{figures/spot.pdf}
\vspace{-0.2cm}
\caption{Electricity's real-time price fluctuates significantly every few minutes.}
\vspace{-0.4cm}
\label{fig:examples}
\end{figure} 

As we outline in Section~\ref{sec:related}, while researchers have recognized that, based on the properties above, HPC and data centers are ideal candidates for demand-side management, there has been little prior research on designing long-running, massively-parallel, and computationally-intensive HPC workloads for power variations. Thus, our goal is to provide insights into designing energy management policies that efficiently execute parallel tasks in the presence of time-varying, dynamic power constraints, which may derive from either the use of local renewable energy sources or participation in a utility demand response program. Our policies dynamically shift available power among a set of nodes executing a parallel application using active and inactive power capping techniques. Our hypothesis is that a policy that combines active power capping---to continuously reallocate available power among nodes based on their real-time utilization---with inactive power capping---to minimize the aggregate power consumption overhead---outperforms other policies in the design space.  In evaluating our hypothesis, we make the following contributions. 

\squishlist

\item {\bf Green Energy Challenges.}  We outline the challenges associated with optimizing parallel applications for green energy sources with variable power.  In particular, we focus on parallel applications that exhibit cross-node dependencies during execution, since these applications complicate both active and inactive power capping.  We then present a model for a representative parallel application that leverages MPI (Message Passing Interface) as a reference point for presenting our energy management policies. 

\item {\bf Dynamic Energy Management Policies.}  We propose dynamic energy management policies that utilize both active and inactive power capping to maximize parallel application performance subject to variable power constraints.  We define dynamic and static policies that apply to both rigid parallel jobs, which can only utilize active power capping, and elastic parallel jobs, which can utilize both active and inactive power capping.  

\item {\bf Implementation and Evaluation.}  We implement our policies on a real prototype cluster, and evaluate their performance using multiple parallel applications.  Our results demonstrate the importance of designing energy agile management policies for variable power. For example, we show that the Graph500 benchmark requires 17\% more time and 9\% more energy to complete when power varies based on real-time electricity prices than the case with unlimited power. 
%versus when unlimited power is available at a fixed price.  
However, since real-time prices are lower than fixed prices, the total cost of our best energy management policy when using real-time prices is 67\% less than when using unlimited power. 

\squishend

Our results show that, for rigid jobs, and representative solar/wind power signals, a dynamic policy, which continuously reallocates power based on node utilization, outperforms a static policy, which only reallocates power when available power changes, by 55\%.  We also show that an elastic task that uses inactive power capping outperforms an equivalent rigid task by 41\%, which demonstrates the importance of elasticity to energy-agile design. \CW{This article is an extended version of our work~\cite{igsc15}. In this article, we have added further clarifications to present our research works in a more comprehensive way. In addition, we added an extensive set of experiments including a detailed evaluation of individual applications, the impact of energy storage capacity, transition time, and overhead power.}

The remainder of this article is organized as follows. In Section~\ref{sec:background}, we define our problem statement, discuss existing mechanisms for node power capping, and present our canonical model of a parallel application.  We then present our energy-agile management policies in Section~\ref{sec:algorithm} and discuss their impact on parallel application performance. Section~\ref{sec:implementation} then details our prototype's implementation, while Section~\ref{sec:evaluation} evaluates our policies on real-world variable power traces.  Finally, Section~\ref{sec:related} puts our work in context with prior research, and Section~\ref{sec:conc} concludes.

%Find that inactive power capping is useful despite (elastic applications)
%
%A feedback loop that reallocates power based on changing per-node utilization (not only when power changes) performs much better. 
%
%
%
%
%
%
%
%
%Our policies determine how to allocate a fixed power 
%
%active and inactive power capping 
%
%propose policies that 
%
%balanced versus greedy 
%
%
%
%
%dynamic versus static allocation of power;
%elastic versus non-elastic 



%To address the problem, we propose a new line of research on energy-agile parallel computing systems that focuses on optimizing parallel tasks to run efficiently despite significant, frequent, and unpredictable power variations, e.g., from changing real-time prices and renewable energy sources.

%\emph{Our hypothesis is that energy-agile systems are capable of exploiting aggressive power optimizations that substantially reduce operational costs, and are complementary to ongoing improvements in energy-efficiency.}  









\begin{comment}

\section{Introduction}
Today's data centers contain tens of thousands of switches and servers. Aiming to provide reliable and flexible services, data centers are usually highly over-provisioned. The extensive use of cloud and HPC computing resources has led to vast amounts of energy consumption, and the cost of energy is predicted to increase in the near future \cite{EnergyReport}. As shown in report \cite{CloudCoal}, the world’s Information-Communications-Technologies (ICT) ecosystem uses about 1,500 TWh of electricity annually, equal to all the electric generation of Japan and Germany combined. The ICT ecosystem now approaches 10\% of world electricity generation. 
%This clearly states the necessity of building data centers that are energy efficient, and/or driven by green energy resources such as wind and solar power.

To reduce the power cost, one way is to improve the energy efficiency, this is achieved by improving power-provisioning technique and building energy proportional servers and/or cluster.
%two main trends are making cluster power proportional and use renewable energy resources.  
Currently, the service and hardware providers has been working hard towards energy proportional servers and data centers. The ideal power-proportional servers consume 0\% power when they are idle, and 100\% of their peak power when running at full workload. This indicates that fully power proportional data centers are fully responsive to the workload. However, even for today, power proportionality is still a challenging goal. \cite{Bhattacharya:2014:UPP:2602044.2602085} shows that, contemporary servers consumes 20-80\% of the peak power even when they are at the idle state. Many approaches have been trying to build power proportional clusters with non-power-proportional servers in the perspective of server power-saving mode \cite{5934885}, optimizing specific applications \cite{Lang:2010:EMM:1920841.1920862}, etc. 

Another way for reducing regular energy consumption is the use of new, green and renewable energy resources. Reports show that leading IT companies have been striving to power their infrastructure with as much renewable energy as possible. Google shows 35\% of their operations are powered by renewable energy, while Facebook has also committed to a goal of 25\% renewable energy for all of its global data centers by the end of 2015~\cite{BigTechRenewable}. However, powering data centers with renewable energy, such as wind and solar power, is still challenging because of its nature of intermittent and unpredictable. For example, it is difficult to predict the amount and strength of wind and sunshine.

The observations above clearly states the importance of building energy agile, demand response data centers, which allows the data centers to be able to react to the available power, especially when they are using renewable energy as main energy resource. Renewable energy driven data center has been discussed intensively, and there has been many research works done for optimizing data center management to be adaptive to intermittent power resources. One way people have done this in the past is they use batteries to store electricity when energy is available and use battery power when the energy resource is unstable or diminished. Another method is to use batch schedulers for deadline scheduling. In this case, the low priority jobs are deferred until there is enough power to finish the tasks.

However, neither of those approaches helps determine how to run large parallel tasks when energy varies. In the first approach, it is very expensive to deploy large scale of batteries; while the second one only works fine when the scale of tasks are relatively small, and the runtime is short: for large scale parallel tasks, the available energy might never be able to power the entire job at once. So we would like to design a system to module the power for the large scale parallel jobs. 

In our work, we look into two kinds of parallel jobs. In the first case, we look into the non-elastic parallel application, i.e., the scale of application can not be sized once it is started. This case commonly exists for High Performance Computing (HPC) applications, e.g., applications based on Massage Passing Interface (MPI)~\cite{Gropp:1994:UMP}. In the second case, we consider the elastic application, i.e., we are able to freely turn worker nodes on and off. 

We show that there is some inherent waste caused by non-elastic jobs because of the high idle power usage. Intuitively, the balanced power allocation approach is considered ideal, since in parallel applications, we would like all the process to be progressive at the same rate. We look at both of the cases and discuss that the intuitive balanced power allocation strategy might not lead to better performance when available power level is low. 


Specifically, we make the following contributions in this article: 1) we look into designing and adapting parallel program under dynamic power constrains. We look at both rigid jobs and elastic. For rigid jobs we show that with active power capping and feedback power control mechanism can enhance the power utilization efficiency. 2) We show that the power management framework can do better when the tasks are elastic. Many research works have been turning the existing non-elastic parallel applications into elastic ones. Most of the reasons are to be able to run the applications on cloud computing platforms, improving fault tolerant capability, etc. In our work, we show the energy perspective for the benefit of improving the elasticity of non-elastic applications.


Our work includes some counter intuitive findings. For example, we show that turning nodes off can perform better in the power varying cases;
%, because more power is wasted when all nodes are on and when power level is low.
Also we show that even if the balanced approach can be beneficial for typical parallel applications, our dynamically adjusting power budget can perform better than the application with strict balanced power.

Our results show that \CW{performance results goes here.}

\end{comment}
%Based on these observations, we propose the ElasticCloud, a green data center management framework aiming to {\it i)} maximize the cluster overall power efficiency and {\it ii)} adapt to the intermittent renewable power resources, especially when the available power is low. ElasicCloud is able to quickly compute the best number of nodes that should be running in real time according to the given power budget. Through micro adjustment feedback control mechanism, ElasticCloud also able to quickly and dynamically adjust power capping for each server, thus can achieve better overall performance on the cluster level. This system is especially beneficial when operating on a coarse control level, e.g., when the controller is only able to control power on the power distribution unit (PDU) level.

%We show two versions of the power optimization algorithm: the first algorithm aims to find the optimal number of nodes that should be powered on, in addition to the power cap that should be allocated to the nodes. This algorithm runs in linear time ($O(n)$); the second algorithm sets up look up a table that pre-calculates the power allocation policy, this makes the algorithm runs in constant time ($O(1)$).

%We profile the power usage of ElasicCloud on a mini cluster with 6 servers. We evaluate the cluster management algorithm with both synthetic, randomly generated power traces, and real wind and solar power traces that are collected by UMass and Holyoke Gas and Electric company. We show that our algorithms are simple yet effective in power efficiency and quick adaptation to the power budget variation especially in the case of renewable energy. To the best of our knowledge, this is the first study to show the optimization of power allocation based on available green energy resources.
