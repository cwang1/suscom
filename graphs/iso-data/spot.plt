set terminal postscript eps enhanced "Helvetica" 25
set output 'spot.eps'
set size 1.0,0.8
set border 3
unset log
unset label
set key top right
set xtics nomirror font "Helvetica,22"
set ytics nomirror font "Helvetica,22"
set ylabel "Spot Price ($/kWh)" font "Helvetica,28"
set xlabel "Time (hours)" font "Helvetica,28"
set yrange [0.02:0.06]
set xrange [0:24]
set xtics 4
plot \
'data.txt' using (24*(($1*5/60)/24-4)):($2/1000) notitle with lines lw 5

