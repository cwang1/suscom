ALL = paper.ps
PS  = paper.ps
PDF = paper.pdf
TEX = paper.tex 
#TEX = paper.tex abstract.tex introduction.tex conclusion.tex

all:
	pdflatex paper
#	pdflatex paper
	bibtex paper
	pdflatex paper
	pdflatex paper


clean:
	yes | rm -f $(PS) $(PDF) *.dvi *.log *.aux *.bbl *.blg
